import DataMining.Functions

def Calculate(training_points, point_for_classification):
    """ returns the calculated class for a given point based on the training data """
    #print(training_points)
    possible_options = DataMining.Functions.determine_possible_options(training_points)
    training_points_probabilities = DataMining.Functions.calculate_probabilities(training_points, possible_options=possible_options)
    class_probabilities = training_points_probabilities["class"]
    #print(classes)
    maginal_distributions = calc_maginal_distributions(training_points, class_probabilities, point_for_classification)
    #print(maginal_distributions)
    classificator = {}
    for c in class_probabilities:
        classificator[c] = round(class_probabilities[c]["probability"]*maginal_distributions[c],3)
    sorted_classificator = {k: v for k, v in sorted(classificator.items(), key=lambda item: item[1])}
    #print(classificator)
    return list(sorted_classificator)[-1]

def calc_maginal_distributions(training_points, classes, point_for_classification):
    """ returns the maginal distributions for the training data based on the values of the point that should be classified """
    conditional_maginal_distributions = {}
    for attribute in  point_for_classification:
        #print(attribute)
            #filters = [[attribute, "class"],[point_for_classification[attribute], c]]
        filters = [[attribute],[point_for_classification[attribute]]]
        training_subset = DataMining.Functions.filter_data(training_points, filters)
        #print(training_subset)
        training_subset_probabilities = DataMining.Functions.calculate_probabilities(training_subset)
        #print(training_subset_probabilities)
        for c in classes:
            #print(training_subset_probabilities["class"][c]["count"] / classes[c]["count"])
            if c not in conditional_maginal_distributions:
                conditional_maginal_distributions[c] = (training_subset_probabilities["class"][c]["count"] / classes[c]["count"])
            else:
                conditional_maginal_distributions[c] *= (training_subset_probabilities["class"][c]["count"] / classes[c]["count"])
    return conditional_maginal_distributions



            