import DataMining.Functions

def Calculate(classification_results):
    possible_classes = DataMining.Functions.determine_possible_options(classification_results)
    for gc in possible_classes["guessed_class"]:
        if gc not in possible_classes["class"]:
            possible_classes["class"].append(gc)
    for c in possible_classes["class"]:
        if c not in possible_classes["guessed_class"]:
            possible_classes["guessed_class"].append(c)
    
    #print(possible_classes)
    binarized_classification_results = binarize_classification_results(classification_results, possible_classes)
    #print(binarized_classification_results)
    binary_confusion_matrix = calc_binary_confusion_matrix(binarized_classification_results, possible_classes)
    #print(binary_confusion_matrix)
    evaluation_measures = calc_evaluation_measures(binary_confusion_matrix, possible_classes)
    #print(evaluation_measures)
    return(evaluation_measures)
   
def binarize_classification_results(classification_results, possible_classes):
    binarized_classification_results = {}
    for point in classification_results:
        binarized_classification_results[point] = {"classes":{}, "guessed_classes":{}}
        for c in possible_classes["class"]:
            if classification_results[point]["class"] == c:
                binarized_classification_results[point]["classes"][c] = "Yes"
            else:
                binarized_classification_results[point]["classes"][c] = "No"
        for c in possible_classes["guessed_class"]:
            if classification_results[point]["guessed_class"] == c:
                binarized_classification_results[point]["guessed_classes"][c] = "Yes"
            else:
                binarized_classification_results[point]["guessed_classes"][c] = "No"
    #print(binarized_classification_results)
    return binarized_classification_results

def calc_binary_confusion_matrix(binarized_classification_results, possible_classes):
    binary_confusion_matrix = {}
    for c in possible_classes["class"]:
        binary_confusion_matrix[c] = {
            "true_positives":0,
            "true_negatives":0,
            "false_positives":0,
            "false_negatives":0,
        }
        for point in binarized_classification_results:
            #print(binarized_classification_results[point]["classes"][c], binarized_classification_results[point]["guessed_classes"][c])
            if binarized_classification_results[point]["classes"][c] == "Yes":
                if binarized_classification_results[point]["guessed_classes"][c] == "Yes":
                    binary_confusion_matrix[c]["true_positives"] += 1
                elif binarized_classification_results[point]["guessed_classes"][c] == "No":
                    binary_confusion_matrix[c]["false_negatives"] += 1
            elif binarized_classification_results[point]["classes"][c] == "No":
                if binarized_classification_results[point]["guessed_classes"][c] == "Yes":
                    binary_confusion_matrix[c]["false_positives"] += 1
                elif binarized_classification_results[point]["guessed_classes"][c] == "No":
                    binary_confusion_matrix[c]["true_negatives"] += 1
    return binary_confusion_matrix


def calc_evaluation_measures(binary_confusion_matrix, possible_classes):
    evaluation_measures = {}
    for c in possible_classes["class"]:
        TP = binary_confusion_matrix[c]["true_positives"]
        TN = binary_confusion_matrix[c]["true_negatives"]
        FP = binary_confusion_matrix[c]["false_positives"]
        FN = binary_confusion_matrix[c]["false_negatives"]
        P = TP + FN
        N = FP + TN
        try:
            precision = (TP) / (TP + FP)
        except:
            precision = 0
        try:
            recall = TP / P
        except:
            recall = 0
        try:
            f_score = (2 * precision * recall) / (precision + recall)
        except:
            f_score = 0
        evaluation_measures[c] = {}
        evaluation_measures[c]["accuracy"] = (TP + TN) / (P + N)
        evaluation_measures[c]["precision"] = precision
        evaluation_measures[c]["recall"] =  recall
        evaluation_measures[c]["f-score"] = f_score
    return evaluation_measures

