import DataMining.Functions

def Calculate(data, classes, new_point, k = 2, normalized = None, distance_function = "Euklid"):
    """ returns the calculated class for a given point based on the distances to k neighbours of the training points """
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    #print(normalized)
    norm_point = DataMining.Functions.normalize_point(new_point, normalize_coefficients = normalized["coefficients"])
    #print(norm_point)
    distances = DataMining.Functions.calc_all_distances(normalized["data"], points = norm_point, distance_function = distance_function)
    #distances = DataMining.Functions.calc_all_distances(data, point = new_point, distance_function = distance_function)
    print(distances)
    #print(classes)
    nearest_neighbours = find_k_nearest_neighbours(distances, k)
    best_class = find_class(classes, nearest_neighbours)
    return best_class


def find_k_nearest_neighbours(distances, k):
    """ returns the k nearest neighbours of a """
    nearest_neighbours = []
    for neighbour in distances:
        #print("neighbour", neighbour)
        if len(nearest_neighbours) < k:
            nearest_neighbours.append(neighbour)
        else:
            farthest_neighbour = nearest_neighbours[0]
            for nn in nearest_neighbours:
                if distances[nn] > distances[farthest_neighbour]:
                    farthest_neighbour = nn
            #print("farthest_neighbour", farthest_neighbour, distances[farthest_neighbour],"neighbour", neighbour, "distances[neighbour]", distances[neighbour])
            if distances[neighbour] < distances[farthest_neighbour]:
                nearest_neighbours.remove(farthest_neighbour)
                nearest_neighbours.append(neighbour)
        #print(nearest_neighbours)
    return (nearest_neighbours)

def find_class(classes, nearest_neighbours):
    """ returns the class based on the nearest neighbours """
    class_count = {}
    for nn in nearest_neighbours:
        for c in classes:
            if c not in class_count:
                class_count[c] = 0
            if nn in classes[c]:
                class_count[c] += 1
    #print(class_count)
    return (max(class_count, key=class_count.get))