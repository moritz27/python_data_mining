import DataMining.Functions
import math, random

def classificator(decision_tree, point_for_classification):
    """ returns the recursive determined best possible class for a given data point based on a decision tree"""
    #print("decision_tree", decision_tree, type(decision_tree))
    #print("point_for_classification", point_for_classification)
    if type(decision_tree) == dict:
        for attribute in decision_tree:
            #if attribute in point_for_classification:
            #print(point_for_classification[attribute])
            try:
                result = classificator(decision_tree[attribute][point_for_classification[attribute]], point_for_classification)
            except:
                result = "unclassified"
                #del point_for_classification[attribute]
        return result
    else:
        return decision_tree

def create_ID3(training_points):
    """ returns a recursive built decision tree based on the training data """
    #print(training_points)
    possible_options = DataMining.Functions.determine_possible_options(training_points)
    training_points_probabilities = DataMining.Functions.calculate_probabilities(training_points, possible_options=possible_options)
    #print(training_points_probabilities)
    entropy = calc_entropy(training_points_probabilities["class"])
    #print(entropy)
    if entropy == 0 :
        rand_key = random.choice(list(training_points.keys()))
        return training_points[rand_key]["class"]
    split_attribute_entropies = calc_split_attribute_entropies(training_points_probabilities)
    #print(split_attribute_entropies)
    attribute_entropies = calc_attribute_entropies(split_attribute_entropies, training_points_probabilities)
    #print(attribute_entropies)
    information_gains = calc_information_gains(entropy, attribute_entropies)
    #print(information_gains)
    best_ig = list(information_gains)[-1]
    if(information_gains[best_ig]) == 0.0:
        #print(information_gains[best_ig])
        return "unclassified"
    decision_tree = {best_ig:{}}
    #print(best_ig)
    for attribute in possible_options[best_ig]:
        filters = [[best_ig],[attribute]]
        training_subset = DataMining.Functions.filter_data(training_points, filters)
        #print(training_subset)
        decision_tree[best_ig][attribute] = create_ID3(training_subset)
    #print(decision_tree)
    return decision_tree

def create_C4_5(training_points):
    """ returns a recursive built decision tree based on the training data """
    #print(training_points)
    possible_options = DataMining.Functions.determine_possible_options(training_points)
    training_points_probabilities = DataMining.Functions.calculate_probabilities(training_points, possible_options=possible_options)
    #print(training_points_probabilities)
    entropy = calc_entropy(training_points_probabilities["class"])
    #print(entropy)
    if entropy == 0 :
        rand_key = random.choice(list(training_points.keys()))
        return training_points[rand_key]["class"]
    split_attribute_entropies = calc_split_attribute_entropies(training_points_probabilities)
    #print(split_attribute_entropies)
    attribute_entropies = calc_attribute_entropies(split_attribute_entropies, training_points_probabilities)
    #print(attribute_entropies)
    information_gains = calc_information_gains(entropy, attribute_entropies)
    #print("information_gains", information_gains)
    best_ig = list(information_gains)[-1]
    if(information_gains[best_ig]) == 0.0:
        #print(information_gains[best_ig])
        return "unclassified"
    split_infos = calc_split_infos(training_points_probabilities)
    #print("split_infos", split_infos)
    information_gain_ratios  = calc_information_gain_ratios(information_gains, split_infos)
    #print("information_gain_ratios", information_gain_ratios)
    best_igr = list(information_gain_ratios)[-1]
    #print("best_igr", best_igr) 
    decision_tree = {best_igr:{}}
    
    for attribute in possible_options[best_igr]:
        filters = [[best_igr],[attribute]]
        training_subset = DataMining.Functions.filter_data(training_points, filters)
        #print(training_subset)
        decision_tree[best_igr][attribute] = create_C4_5(training_subset)
    #print(decision_tree)
    return decision_tree
    

def calc_entropy(probabilities):
    """ returns the entropy of given probabilities """
    entropy = 0
    for attribute in probabilities:
        if probabilities[attribute]["probability"] != 0:
            entropy -= probabilities[attribute]["probability"] * math.log2(probabilities[attribute]["probability"])
    return entropy

def calc_split_attribute_entropies(probabilities):
    """ returns the entropies of all given single attribute probabilities """
    split_attribute_entropies = {}
    for attribute in probabilities:
        if attribute != "class":
            split_attribute_entropies[attribute] = {}
            for split_attribute in probabilities[attribute]:
                #print(probabilities[attribute][split_attribute]["class"])
                split_attribute_entropies[attribute][split_attribute] = calc_entropy(probabilities[attribute][split_attribute]["class"])
                #print(probabilities[split_attribute])
    return split_attribute_entropies

def calc_attribute_entropies(split_attribute_entropies, probabilities):
    """ returns the sum of all entropies probabilities """
    attribute_entropies = {}
    for attribute in split_attribute_entropies:
        #print(attribute)
        attribute_entropies[attribute] = 0
        for split_attribute in split_attribute_entropies[attribute]:
            #print("  ", split_attribute)
            attribute_entropies[attribute] += probabilities[attribute][split_attribute]["probability"] * split_attribute_entropies[attribute][split_attribute]
    return attribute_entropies


def calc_information_gains(entropy, attribute_entropies):
    """ returns the information gains of the attributes """
    information_gains = {}
    for attribute in attribute_entropies:
        information_gains[attribute] = round((entropy - attribute_entropies[attribute]),3)
    return {k: v for k, v in sorted(information_gains.items(), key=lambda item: item[1])}

def calc_split_infos(probabilities):
    split_infos={}
    for attribute in probabilities:
        if attribute != "class":
            #print(attribute)
            split_infos[attribute] = 0
            for split_attribute in probabilities[attribute]:
                probability = probabilities[attribute][split_attribute]["probability"]
                #print("   ", split_attribute, probability)
                split_infos[attribute] -= probability * math.log(probability,2)
    return split_infos
    

def calc_information_gain_ratios(information_gains, split_infos):
    """ returns the information gain ratios of the attributes """
    information_gain_ratios = {}
    for attribute in information_gains:
        #print(information_gains[attribute], split_infos[attribute])
        if information_gains[attribute] != 0 and split_infos[attribute] != 0:
            information_gain_ratios[attribute] = round((information_gains[attribute] / split_infos[attribute]),3)
        else:
            information_gain_ratios[attribute] = 0
    return {k: v for k, v in sorted(information_gain_ratios.items(), key=lambda item: item[1])}

def check_which_is_pure_class(classes):
    """ returns the pure class which only contains the same values for one attribute """
    for c in classes:
        if classes[c]["probability"] == 1.0:
            return c

    