import matplotlib.pyplot as plt
import numpy as np
import random, statistics
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
from scipy.spatial import ConvexHull

def Histogramm(data, n_bins = 20):
    """ displays a histogramm of the given data"""
    fig, axs = plt.subplots(1, tight_layout=True)
    axs.hist(data, bins=n_bins)
    plt.show()

def Clusters_2d(data, clusters, plt_name = None, centroids = None, add_point = None, show_names = False):
    """ displays a 2-D Cluster Plot """
    plt.figure("2-D Cluster Plot")
    if plt_name != None:
        plt.title(plt_name)
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    plot_data = {}
    plot_point_names = []
    #print(clusters)
    for c in clusters:
        plot_data[c] = [[] for i in range(dimensions)]
        #print(plot_data)
        for p in clusters[c]:
            #print(p, data[p])
            plot_point_names.append(p)
            for s in range(0, dimensions):
                #print(s, data[p][s])
                plot_data[c][s].append(data[p][s])
    #print(plot_data, plot_point_names)
    counter = 0
    cluster_counter = 0
    x_min = None
    x_max = None
    y_min = None
    y_max = None

    for pd in plot_data:
        r = random.random()
        b = random.random()
        g = random.random()
        c = (r, g, b)
        if show_names == True:
            plt.scatter(plot_data[pd][0],plot_data[pd][1], s=150, facecolors='none', edgecolors=c ,label=pd) 
        else:
            plt.scatter(plot_data[pd][0],plot_data[pd][1], marker="D", color=c ,label=pd) 
        if centroids != None:
            plt.scatter(centroids[cluster_counter][0],centroids[cluster_counter][1], s=50, color =c) 
        # plt.annotate(pd, # this is the text
        #                 (centroids[cluster_counter][0],centroids[cluster_counter][1]), # this is the point to label
        #                 textcoords="offset points", # how to position the text
        #                 xytext=(0,-3.5), # distance from text to points (x,y)
        #                 ha='center') # horizontal alignment can be left, right or center

        x = np.array(plot_data[pd][0]) 
        y = np.array(plot_data[pd][1])
        xy = np.hstack((x[:,np.newaxis],y[:,np.newaxis]))
        try:
            hull = ConvexHull(xy)
            for simplex in hull.simplices:
                plt.plot(x[simplex], y[simplex], color=c)
        except:
            pass
            #print("not enough points for hull")
        
        
        for p in range (0, len(plot_data[pd][0])):
            if show_names == True:
                plt.annotate(plot_point_names[counter], # this is the text
                            (plot_data[pd][0][p],plot_data[pd][1][p]), # this is the point to label
                            textcoords="offset points", # how to position the text
                            xytext=(0,-3.5), # distance from text to points (x,y)
                            ha='center') # horizontal alignment can be left, right or center
            if x_max == None or plot_data[pd][0][p] > x_max:
                x_max = plot_data[pd][0][p]
            if x_min == None or plot_data[pd][0][p] < x_min:
                x_min = plot_data[pd][0][p]
            if y_max == None or plot_data[pd][1][p] > y_max:
                y_max = plot_data[pd][1][p]
            if y_min == None or plot_data[pd][1][p] < y_min:
                y_min = plot_data[pd][1][p]
            counter+=1
        cluster_counter+=1

    #x_max += 1
    #y_max += 1

    # for centroid in centroids:
    #     print(centroid)
    #     plt.scatter(centroid[0],centroid[1], s=150, color = "b") 

    #print(x_min, x_max, y_min, y_max)

    if add_point != None:
        plt.scatter(add_point[0],add_point[1], s=50, c = "k", marker= "x") 

    plt.legend()
    plt.xticks(np.linspace(x_min, x_max, 11, dtype=int, endpoint = True)) 
    plt.yticks(np.linspace(y_min, y_max, 11, dtype=int, endpoint = True)) 
    plt.grid()
    plt.show()

def All_Clusters_2d(data, clusters, plt_name = None, centroids = None, show_names = False, ax = None):
    """ returns a 2-D Cluster Plot for multi-plotting"""
    if ax is None:
        ax = plt.gca()
    if plt_name != None:
        sub_plot = ax.title.set_text(plt_name)
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    plot_data = {}
    plot_point_names = []
    #print(clusters)
    for c in clusters:
        plot_data[c] = [[] for i in range(dimensions)]
        #print(plot_data)
        for p in clusters[c]:
            #print(p, data[p])
            plot_point_names.append(p)
            for s in range(0, dimensions):
                #print(s, data[p][s])
                plot_data[c][s].append(data[p][s])
    #print(plot_data, plot_point_names)
    counter = 0
    cluster_counter = 0
    x_min = None
    x_max = None
    y_min = None
    y_max = None

    for pd in plot_data:
        r = random.random()
        b = random.random()
        g = random.random()
        c = (r, g, b)
        if show_names == True:
            sub_plot = ax.scatter(plot_data[pd][0],plot_data[pd][1], s=150, facecolors='none', edgecolors=c ,label=pd) 
        else:
            sub_plot = ax.scatter(plot_data[pd][0],plot_data[pd][1], marker="D", color=c ,label=pd) 
        if centroids != None:
            sub_plot = ax.scatter(centroids[cluster_counter][0],centroids[cluster_counter][1], s=50, color =c) 
        # plt.annotate(pd, # this is the text
        #                 (centroids[cluster_counter][0],centroids[cluster_counter][1]), # this is the point to label
        #                 textcoords="offset points", # how to position the text
        #                 xytext=(0,-3.5), # distance from text to points (x,y)
        #                 ha='center') # horizontal alignment can be left, right or center

        x = np.array(plot_data[pd][0]) 
        y = np.array(plot_data[pd][1])
        xy = np.hstack((x[:,np.newaxis],y[:,np.newaxis]))
        try:
            hull = ConvexHull(xy)
            for simplex in hull.simplices:
                sub_plot = ax.plot(x[simplex], y[simplex], color=c)
        except:
            pass
            #print("not enough points for hull")
        
        
        for p in range (0, len(plot_data[pd][0])):
            if show_names == True:
                sub_plot = ax.annotate(plot_point_names[counter], # this is the text
                            (plot_data[pd][0][p],plot_data[pd][1][p]), # this is the point to label
                            textcoords="offset points", # how to position the text
                            xytext=(0,-3.5), # distance from text to points (x,y)
                            ha='center') # horizontal alignment can be left, right or center
            if x_max == None or plot_data[pd][0][p] > x_max:
                x_max = plot_data[pd][0][p]
            if x_min == None or plot_data[pd][0][p] < x_min:
                x_min = plot_data[pd][0][p]
            if y_max == None or plot_data[pd][1][p] > y_max:
                y_max = plot_data[pd][1][p]
            if y_min == None or plot_data[pd][1][p] < y_min:
                y_min = plot_data[pd][1][p]
            counter+=1
        cluster_counter+=1

    sub_plot = ax.legend()
    print(x_min, x_max)
    ax.set_xticks(np.linspace(x_min, x_max, 11, dtype=int, endpoint = True)) 
    ax.set_yticks(np.linspace(y_min, y_max, 11, dtype=int, endpoint = True))
    #sub_plot = ax.title("2-D Cluster Plot")
    sub_plot = ax.grid()
    return sub_plot

def Reachability_Chart(distances, reachability_distances, norm_data, start_point = None, name = None):
    """ displays a Reachability Chart like OPTICS """
    plot_point_names = []
    plot_point_values = [0.5]
    if start_point == None:
        p1 = random.choice(list(reachability_distances.keys()))
    else:
        p1 = start_point
    plot_point_names.append(p1)
    #print(plot_point_names)
    for i in reachability_distances:
    #for i in range (0,5):
        lowest_reachability_value = None
        lowest_reachability_point = None
        last_lowest_distance_value = None
        #last_lowest_distance_point = None
        for p2 in reachability_distances[p1]:
            if p2 not in plot_point_names:
                if lowest_reachability_value == None or reachability_distances[p1][p2] <= lowest_reachability_value:
                    if last_lowest_distance_value == None or distances[p1][p2] < last_lowest_distance_value:
                        lowest_reachability_value = reachability_distances[p1][p2]
                        lowest_reachability_point = p2
                        last_lowest_distance_value = distances[p1][p2]
            #print(p1, p2, reachability_distances[p1][p2], last_lowest_distance_value, distances[p1][p2])
        #print(lowest_reachability_point, "->",  plot_point_names)
        if lowest_reachability_point not in plot_point_names and lowest_reachability_point != None:
            plot_point_names.append(lowest_reachability_point)
            plot_point_values.append(lowest_reachability_value)
            p1 = lowest_reachability_point

    #print(plot_point_names, plot_point_values)

    if name == None:
        name = "Erreichbarkeitsdiagramm"

    #print(plot_point_values)

    for i in range (0, len(plot_point_values)):
        plot_point_values[i] = plot_point_values[i] * statistics.mean(norm_data["spans"])  

    plt.figure(name)
    plt.title(name)
    plt.grid()
    plt.bar(plot_point_names, plot_point_values, linewidth=.1, joinstyle="round", align='center')
    plt.show()

    return

def Plot_Points(data, add_points = None, show_names = False, show_add_names = False):
    X, Y, plot_point_names = [],[],[]
    for point in data:
        X.append(data[point][0])
        Y.append(data[point][1])
        plot_point_names.append(point)
    fig, ax = plt.subplots()
    r = random.random()
    b = random.random()
    g = random.random()
    c = (r, g, b)
    if show_names == True:
        ax.scatter(X, Y, s=150, facecolors='none', edgecolors=c)
        for i, txt in enumerate(plot_point_names):
            ax.annotate(txt, (X[i], Y[i]), textcoords="offset points", xytext=(0,-3.5), ha='center')
    else:
        ax.scatter(X, Y, marker="D")

    if add_points != None:
        X_2, Y_2, plot_point_names_2 = [],[],[]
        for point in add_points:
            X_2.append(add_points[point][0])
            Y_2.append(add_points[point][1])
            plot_point_names_2.append(point)
        ax.scatter(X_2, Y_2, marker="D")
        if show_add_names == True:
            for i, txt in enumerate(plot_point_names_2):
                ax.annotate(txt, (X_2[i], Y_2[i]), textcoords="offset points", xytext=(0,+8), ha='center', fontsize=7)
    plt.grid()
    plt.show()


    