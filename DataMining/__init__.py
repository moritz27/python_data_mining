#pip3 install numpy scipy matplotlib beautifultable
# import random, statistics, collections, json
# import numpy as np
# from scipy.spatial import ConvexHull
# import matplotlib.pyplot as plt
# import itertools as it


import DataMining.Functions
import DataMining.Correlation
import DataMining.Distance 
import DataMining.Similarity
import DataMining.Clustering.K_Means
import DataMining.Clustering.K_Medoids
import DataMining.Clustering.K_Modes
import DataMining.Clustering.Linkage
import DataMining.Clustering.ROCK
import DataMining.Clustering.Optics
import DataMining.Clustering.DB_Scan
import DataMining.Clustering.DB_Scan_recursive
import DataMining.Clustering.CLIQUE
import DataMining.Clustering.CLIQUE_NEW
import DataMining.Clustering.Expectation_Maximization
import DataMining.Clustering.Hopkins_Statistic
import DataMining.Classification.K_Nearest_Neighbours
import DataMining.Classification.Decision_Trees
import DataMining.Classification.Naive_Bayes
import DataMining.Classification.Rate_Results
import DataMining.Association_Rules

import DataMining.Plotting