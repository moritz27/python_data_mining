import math

def pth_root(x,p):
    """ returns the pth root of the number x """
    return x**(1/float(p))

def Cityblock(X,Y, round_value = None):
    """ returns the cityblock distance between X and Y """
    distance = 0
    p = len(X)
    for i in range (0,p):
        distance += abs(X[i] - Y[i])
        #print(X[i], Y[i], abs(X[i] - Y[i]), distance)
    if distance > 0:
        if round_value != None:
            return round(distance, round_value)
        else:
            return distance
    else:
        return 0

def Euklid(X,Y, round_value = 2):
    """ returns the euklid distance between X and Y """
    distance = 0
    p = len(X)
    for i in range (0,p):
        distance += ((abs(X[i] - Y[i]))**2)
        #print("X[i]", X[i],"Y[i]", Y[i],"abs(X[i] - Y[i])", abs(X[i] - Y[i]),"distance", distance)
    if distance > 0:
        return round(math.sqrt(distance), round_value)
    else:
        return 0

def Max_Norm(X,Y, round_value = None):
    """ returns the max distance between X and Y """
    max_distance = 0
    p = len(X)
    for i in range (0,p):
        distance = abs(X[i] - Y[i])
        if distance > max_distance:
            max_distance = distance
    return max_distance

def Hamming(data,X,Y, round_value = None):
    """ returns the hamming distance between X and Y in the given data"""
    distance = 0
    for attribute in data[X]:
        try:
            if data[X][attribute] != data[Y][attribute]:
                distance += 1
        except:
            #print(Y)
            if data[X][attribute] != Y[attribute]:
                distance += 1
    return distance