import random, math, json, operator, copy, statistics, csv
import DataMining.Distance
from beautifultable import BeautifulTable
from functools import reduce  # Python 3 forward compatibility 
import pandas as pd
import itertools as it

def read_by_index(lst, indexes):
    """ returns the value of the list by the index list"""
    return reduce(operator.getitem, indexes, lst)   

def write_by_index(lst, new_value, append = False, indexes=None):
    """ writes to the position of the list by the index list """
    if indexes and len(indexes) > 0:
        target = read_by_index(lst, indexes[:-1])
        if append == True:
            target[indexes[-1]].append(new_value)
        else:
            target[indexes[-1]] = new_value
    else:
        lst = new_value
    return lst

def filter_data(data, filters):
    """ removes all items that do not match all filters """ 
    filtered_data = {}
    for point in data:
        add_point = 1
        for i in range (0, len(filters[0])):
            #print(point, filters[0][i], filters[1][i], data[point])
            if filters[0][i] in data[point]:
                if data[point][filters[0][i]] != (filters[1][i]):
                    add_point = 0
                    #break
        if add_point == 1:
            filtered_data[point] = data[point]
    return filtered_data

def remove_attribute(data, attribute):
    """ removes the given attribute (item) from data and returns a new object, the old data stays the same """
    cleaned_data = copy.deepcopy(data)
    for point in data:
        print(attribute, cleaned_data[point])
        try:
            del cleaned_data[point][attribute]
        except:
            print("error")
    return cleaned_data

def determine_possible_options(data):
    """ returns all possible attribute of objects from given data set """

    rand_key = random.choice(list(data.keys()))

    if type(data[rand_key]) == dict:
        possible_options = {}
        for point in data:
            #print(data[point])
            for option in data[point]:
                #print(option, data[point][option])
                if option not in possible_options:
                    possible_options[option] = []
                if data[point][option] not in possible_options[option]:
                    possible_options[option].append(data[point][option])
    else:
        possible_options = []
        for point in data:
            #print(data[point])
            for i in range (0, len(data[point])):
                #print(i, data[point][i])
                if data[point][i] not in possible_options:
                    possible_options.append(data[point][i])
                
    #print(possible_options)
    return possible_options
    
def count_all_occurences(data, possible_options = None):
    """ returns how often attributes occure in the data """
    if possible_options == None:
        possible_options = determine_possible_options(data)

    rand_key = random.choice(list(data.keys()))
    #data_point_num = len(data)
    #print(data_point_num)
    occurences = {}
    if type(data[rand_key]) == dict:
        for attribute in possible_options:
            occurences[attribute] = {}
            #print(attribute,":")
            for value in possible_options[attribute]:
                occurences[attribute][value] = count_occurrence(data, value, attribute = attribute)
                #print (value, count_occurrence(data, attribute, value))
    else:
        for value in possible_options:
            occurences[value] = count_occurrence(data, value)
    return(occurences)
    
def count_occurrence(data, value, attribute = None, class_condition = None):
    """ returns how often one specific attribute occurs """
    occurrence_counter = 0
    rand_key = random.choice(list(data.keys()))
    for point in data:
        #print(data[point], value, attribute, class_condition)
        if type(data[rand_key]) == dict:
            if data[point][attribute] == value:
                if class_condition == None :
                    occurrence_counter += 1
                elif data[point]["class"] == class_condition:
                    occurrence_counter += 1
        else:
            #print(data[point], value)
            if value in data[point]:
                occurrence_counter += 1
    return occurrence_counter

def calculate_probabilities(data, possible_options = None):
    """ returns the probabilities of all attributes in the data """
    if possible_options == None:
        possible_options = determine_possible_options(data)
    #print(possible_options)
    data_point_num = len(data)
    #print(data_point_num)
    rand_key = random.choice(list(data.keys()))
    option_probabilities = {}
    if type(data[rand_key]) == dict:
        for attribute in possible_options:
            option_probabilities[attribute] = {}
            #print(attribute,":")
            for value in possible_options[attribute]:
                option_probabilities[attribute][value] = {}
                occurrence = count_occurrence(data, value, attribute = attribute)
                option_probabilities[attribute][value]["count"] = occurrence
                option_probabilities[attribute][value]["probability"] = occurrence / data_point_num
                #print (value, count_occurrence(data, attribute, value))
                if "class" in possible_options:
                    if attribute != "class":
                        option_probabilities[attribute][value]["class"] = {}
                        sum = 0
                        for c in possible_options["class"]:
                            occurrence = count_occurrence(data, value, attribute = attribute, class_condition=c)
                            option_probabilities[attribute][value]["class"][c] = {"count": occurrence}
                            sum += occurrence
                        for c in possible_options["class"]:
                            option_probabilities[attribute][value]["class"][c]["probability"] = option_probabilities[attribute][value]["class"][c]["count"] /sum
                        #print(sum)
    else:
        for value in possible_options:
            occurrence = count_occurrence(data, value)
            #print(value, occurrence)
            option_probabilities[value] = {}
            option_probabilities[value]["count"] = occurrence
            option_probabilities[value]["probability"] = occurrence / data_point_num

    return(option_probabilities)

def calc_normalization_coefficients(data, attribute = None):
    """ returns parameters for the nomralization """ 
    rand_key = random.choice(list(data.keys()))
    datatypes = {} 
    extrema = {}
    if attribute == None:  
        dimensions = len(data[rand_key])
        extrema["max_values"] = [None for i in range(dimensions)]
        extrema["min_values"] = [None for i in range(dimensions)]
        extrema["power"] = [None for i in range(dimensions)]  
        for point in data:
            for dimension in range(0, dimensions):
                if extrema["max_values"][dimension] == None or data[point][dimension] > extrema["max_values"][dimension]:
                    extrema["max_values"][dimension] = data[point][dimension]
                if extrema["min_values"][dimension] == None or data[point][dimension] < extrema["min_values"][dimension]:
                    extrema["min_values"][dimension] = data[point][dimension]
                if abs(extrema["min_values"][dimension]) > abs(extrema["max_values"][dimension]):
                    extrema["power"][dimension] = 10 ** math.log10(abs(extrema["min_values"][dimension]))
                elif abs(extrema["max_values"][dimension]) > abs(extrema["min_values"][dimension]):
                    extrema["power"][dimension] = 10 ** math.floor(math.log10(abs(extrema["max_values"][dimension])))
                datatypes[dimension] = type(data[point][dimension])
        #print(extrema)
    else:
        dimensions = len(data[rand_key][attribute])
        extrema["max_values"] = [None for i in range(dimensions)]
        extrema["min_values"] = [None for i in range(dimensions)]
        extrema["power"] = [None for i in range(dimensions)]  
        for point in data:
            for dimension in range(0, dimensions):
                if extrema["max_values"][dimension] == None or data[point][attribute][dimension] > extrema["max_values"][dimension]:
                    extrema["max_values"][dimension] = data[point][attribute][dimension]
                if extrema["min_values"][dimension] == None or data[point][attribute][dimension] < extrema["min_values"][dimension]:
                    extrema["min_values"][dimension] = data[point][attribute][dimension]
                if abs(extrema["min_values"][dimension]) > abs(extrema["max_values"][dimension]):
                    extrema["power"][dimension] = 10 ** math.log10(abs(extrema["min_values"][dimension]))
                elif abs(extrema["max_values"][dimension]) > abs(extrema["min_values"][dimension]):
                    extrema["power"][dimension] = 10 ** math.floor(math.log10(abs(extrema["max_values"][dimension])))
                datatypes[dimension] = type(data[point][attribute][dimension])
        #print(extrema)

    offsets = [0 for i in range(dimensions)]
    spans = [0 for i in range(dimensions)]
    
    for dimension in range(0, dimensions):
        if extrema["max_values"][dimension] < 0:
            offsets[dimension] = abs(extrema["min_values"][dimension])
            spans[dimension] = abs(extrema["min_values"][dimension]) - abs(extrema["max_values"][dimension])
        else:
            if extrema["min_values"][dimension] < 0:
                spans[dimension] = abs(extrema["min_values"][dimension]) + extrema["max_values"][dimension]
                offsets[dimension] = abs(extrema["min_values"][dimension])
            else:
                spans[dimension] = abs(extrema["max_values"][dimension])
                     
    #print(offsets,spans)

    coefficients = {
        "offsets": offsets,
        "mean_offset" : statistics.mean(offsets),
        "spans": spans,
        "mean_span" : statistics.mean(spans),
        "extrema": extrema,
        "mean_power" : statistics.mean(extrema["power"]),
        "datatypes": datatypes
    } 
    
    return coefficients

def calc_normalization_coefficients_list(data):
    """ returns parameters for the nomralization """   
    extrema = {}
    extrema["max_value"] = max(data) 
    extrema["min_value"] = min(data)
    #extrema["power"] = 10 ** math.log10(abs(extrema["min_value"]))
    extrema["power"] = 10 ** math.floor(math.log10(abs(extrema["max_value"])))
    datatype = type(data[0])

    if extrema["max_value"] < 0:
        offset = abs(extrema["min_value"])
        span = abs(extrema["min_value"]) - abs(extrema["max_value"])
    else:
        if extrema["min_value"] < 0:
            span = abs(extrema["min_value"]) + extrema["max_value"]
            offset= abs(extrema["min_value"])
        else:
            span = abs(extrema["max_value"])
            offset = 0
                     
    #print(offsets,spans)

    coefficients = {
        "offset": offset,
        "spans": span,
        "extrema": extrema,
        "datatype": datatype
    } 
    
    return coefficients


def normalize_value(value, data = None, normalize_coefficients = None, backwards = False, round_value = None):
    """ returns a value converted to/from normalized range """ 
    if normalize_coefficients == None:
        normalize_coefficients = calc_normalization_coefficients(data) 
    if backwards == False:
        if round_value != None:
            norm_value = round((value + normalize_coefficients["mean_offset"]) / normalize_coefficients["mean_span"],round_value)
        else:
            norm_value = (value + normalize_coefficients["mean_offset"]) / normalize_coefficients["mean_span"]
    elif backwards == True:
        norm_value = (normalize_coefficients["datatypes"][0])(round_up(((value - normalize_coefficients["mean_offset"]) * normalize_coefficients["mean_span"]), normalize_coefficients["mean_power"]/100))
    #print("(", value, "+", normalize_coefficients["mean_offset"], ") /", normalize_coefficients["mean_span"], "=" , norm_value)
    return norm_value


def normalize_point(point, data = None, normalize_coefficients = None, backwards = False, round_value = None):
    """ returns one point converted to/from normalized range """ 
    if normalize_coefficients == None:
        normalize_coefficients = calc_normalization_coefficients(data)
    dimensions = len(point)
    norm_point = []
    for dimension in range(0, dimensions):
            #print(data[point][dimension], "+", offsets[dimension],"=", (data[point][dimension] + offsets[dimension]), "/", spans[dimension], "=", (data[point][dimension] + offsets[dimension]) / spans[dimension] )
            if backwards == False:
                if round_value != None:
                    norm_point.append(round((point[dimension] + normalize_coefficients["offsets"][dimension]) / normalize_coefficients["spans"][dimension], round_value))
                else:
                    norm_point.append((point[dimension] + normalize_coefficients["offsets"][dimension]) / normalize_coefficients["spans"][dimension])
            elif backwards == True:
                norm_point.append((normalize_coefficients["datatypes"][dimension])(round_up(((point[dimension] - normalize_coefficients["offsets"][dimension]) * normalize_coefficients["spans"][dimension]), normalize_coefficients["extrema"]["power"][dimension]/100)))
    return norm_point


def normalize_data(data, normalize_coefficients = None, backwards = False, round_value = None):
    """ returns the data (dict or nested list) converted to/from normalized range """ 
    if normalize_coefficients == None:
        normalize_coefficients = calc_normalization_coefficients(data)
    norm_data = None
    for point in data:
        if type(point) == list:
            if norm_data == None:
                norm_data = []
            norm_data.append(normalize_point(point, normalize_coefficients = normalize_coefficients, backwards = backwards, round_value = round_value))
        else:
            if norm_data == None:
                norm_data = {}
            norm_data[point] = normalize_point(data[point], normalize_coefficients = normalize_coefficients, backwards = backwards, round_value = round_value)
    result = {
        "data": norm_data,
        "coefficients": normalize_coefficients
    }
    return result


def calc_all_distances(data, points = None, distance_function = "Euklid"):
    """ returns the distances between each points of a data set or a given point and all points of the data"""
    distances = {}
    method_to_call = getattr(DataMining.Distance, distance_function)
    for p1 in data:
        distances[p1] = {}
        if points == None:
            for p2 in data:
                if p1 != p2:
                    #distance = globals()[distance_function](data[p1], data[p2])
                    #distance = method_to_call(data[p1], data[p2])
                    #print(p1, p2, distance)
                    distances[p1][p2] = method_to_call(data[p1], data[p2])
        elif type(points) == list:
            distances[p1] = method_to_call(data[p1], points)
        elif type(points) == dict:
            for p2 in points:
                distances[p1][p2] = method_to_call(data[p1], points[p2])
    return distances

def calc_mean_of_data(data, cluster):
    """ returns the mean value of the data """
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    sums = dimensions*[0]
    #print("c:",c, "sums:", sums)
    for point in cluster:
        #print("point:", point,"data[point]:", data[point])
        for d in range(0, dimensions):
            #print(d, sums, data[point][d])
            sums[d] += data[point][d]
        #print(point, data[point])
    means = []
    for s in range(0, dimensions):
        means.append(sums[s]/len(cluster))
    #print(sums,len(clusters[c]), means)
    #new_centroids.append(statistics.mean(clusters[c]))
    return means

def read_csv_from_file(path, split_char = "\s+"):
    """ returns object from CSV file """
    # use \t+ for tabulator 
    import re
    data = {}
    f = open(path, 'r')
    Lines = f.readlines()
    count = 0
    # Strips the newline character
    for line in Lines:
        count += 1
        name = "DP"+str(count)
        data[name] = []
        point = re.split(split_char, line.strip())
        #print(point)
        for dim in range(0, len(point)):
            data[name].append(int(point[dim]))
        #print("Line{}: {}".format(count, line.strip()))
    return data


def read_cat_csv_from_file(path, split_char):
    import re
    data = {}
    f = open(path, 'r')
    Lines = f.readlines()
    count = 0
    attributes = re.split(split_char, Lines[0])
    #print(str(attributes[-1][-1:]))
    if str(attributes[-1][-1:]) == "\n":
        attributes[-1] = str(attributes[-1][:-1])
    #print(attributes)
    for line in Lines[1:]:
        count += 1
        name = "DP"+str(count)
        data[name] = {}
        values = re.split(split_char, line.strip())
        #print(values)
        for num in range (0, len(attributes)):
            try:
                data[name][str(attributes[num])] = float(values[num])
            except:
                data[name][str(attributes[num])] = str(values[num])
        
        #print("Line{}: {}".format(count, line.strip()))
    return data


def read_json_from_file(path):
    """ returns object from JSON file """
    data = {}
    f = open(path, 'r')
    data = json.load(f)
    return data

def read_data_from_file(path, split_char = "\s+", max_length = 100000):
    # use \t+ for tabulator 
    import re
    data = {}
    f = open(path, 'r')
    Lines = f.readlines()
    count = 0
    # Strips the newline character
    for line in Lines:
        count += 1
        name = "DP"+str(count)
        data[name] = []
        point = re.split(split_char, line.strip())
        #print(point)
        for dim in range(0, len(point)):
            data[name].append(int(point[dim]))
        #print("Line{}: {}".format(count, line.strip()))
        if count >= max_length:
            break
    return data

def print_data_in_table(data , possible_options = None, dp_name = "Datapoint"):
    """ prints the data in a table to the console """
    #print("Data:")
    table = BeautifulTable(maxwidth=120)
    if possible_options == None:
        possible_options = determine_possible_options(data)
    #print(possible_options)

    header = ["Datapoint"]
    for attribute in possible_options:
        header.append(attribute)
    table.rows.append(header)

    for dp in data:
        line_data = [dp]
        for attribute in data[dp]:
            line_data.append(data[dp][attribute])
        table.rows.append(line_data)

    print(table)

def convert_to_tuples(data):
    for point in data:
        data[point] = tuple(data[point])

def convert_to_sets(data):
    for point in data:
        data[point] = set(data[point])

def get_key_by_max_value(dictonary):
    return max(dictonary.items(), key=operator.itemgetter(1))[0]

def key_of_max_value(dictonary):
     """ a) create a list of the dict's keys and values; 
         b) return the key with the max value"""  
     v=list(dictonary.values())
     k=list(dictonary.keys())
     return k[v.index(max(v))]

def key_of_min_value(dictonary):
     """ a) create a list of the dict's keys and values; 
         b) return the key with the min value"""  
     v=list(dictonary.values())
     k=list(dictonary.keys())
     return k[v.index(min(v))]


def pick_n_points(data, n = 5, remove = False):
    picked_points = {}
    while len(picked_points) < n:
        rand_key = random.choice(list(data.keys()))
        if rand_key not in picked_points:
            picked_points[rand_key] = data[rand_key]
            if remove == True:
                del(data[rand_key])
    return picked_points

def split_data_in_n_parts(data, n=None, num=None, part_names = "part_"):
    data_to_split = copy.deepcopy(data)
    if type(part_names) == str:
        if n == None:
            n = 2
        if num == None:
            num = math.floor(len(data)/n)
        split_data = {(part_names+str(i+1)):{} for i in range(0,n)}
    elif type(part_names) == list:
        if n == None:
            n = len(part_names)
        if num == None:
            num = math.floor(len(data)/n)
        split_data = {(part_names[i]):{} for i in range(0,n)}
    
    #print(len(data),n, num)
    split_cnt = 0
    for part in split_data:
        #print(data_to_split, num)
        if type(num) == int:
            points = pick_n_points(data_to_split, num, remove=True)
            #print(points)
            split_data[part] = points
        elif type(num) == list:
            count = math.floor(num[split_cnt] * len(data))
            split_data[part] = pick_n_points(data_to_split, count, remove=True)
            split_cnt +=1

    #print("data_to_split", data_to_split)
    #print(split_data)
    return split_data


def round_up(num, factor):
    """ returns a number rounded up to the given factor """
    return int(math.ceil(num / factor)*factor) + factor

def round_down(num, factor):
    """ returns a number rounded down to the given factor """
    return int(math.floor(num / factor)*factor)

def create_evenly_distributed_points(data, normalized = None, cell_step_sizes = None, normalized_return = False):
    if normalized == None:
        normalized = normalize_data(data)
    evenly_distributed_steps = create_evenly_distributed_steps(data, normalized, cell_step_sizes)
    evenly_distributed_coords = list(it.product(evenly_distributed_steps[0], evenly_distributed_steps[1]))
    #print(evenly_distributed_coords)

    evenly_distributed_points = {}
    for coord in evenly_distributed_coords:
        point_name = "EP" + str(len(evenly_distributed_points)+1)
        #print(point_name, coord)
        evenly_distributed_points[point_name] = list(coord)
    #print(evenly_distributed_points)
    #print(normalized)
    if normalized_return == True:
        normalized_evenly_distributed_points = normalize_data(evenly_distributed_points, normalized["coefficients"])
        #print(normalized_evenly_distributed_points)
        return normalized_evenly_distributed_points
    else:
        return evenly_distributed_points

def create_evenly_distributed_steps(data, normalized = None, cell_step_sizes = None):
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    if normalized == None:
        normalized = normalize_data(data)
    evenly_distributed_steps = [[] for i in range(dimensions)]
    for dimension in range (0,dimensions):
        min_val = normalized["coefficients"]["extrema"]["min_values"][dimension]
        max_val = normalized["coefficients"]["extrema"]["max_values"][dimension]
        if cell_step_sizes == None:
            step =  normalized["coefficients"]["extrema"]["power"][dimension]
        else:
            step = cell_step_sizes[dimension]
            #print(step)
        evenly_distributed_steps[dimension] = list(range(round_down(min_val, step), round_up(max_val, step), step))

def discretize_values(data, evenly_distributed_steps = None, intervals = 3):
    numerical_attributes = {}
    categorial_attributes = {}
    for point in data:
        numerical_attributes[point] = {}
        categorial_attributes[point] = {}
        #print(data[point])
        attribute_list =[]
        for attribute in data[point]:
            if type(data[point][attribute]) == int or type(data[point][attribute]) == float:
                # if attribute not in numerical_attributes:
                #     numerical_attributes[attribute] = []
                if attribute not in attribute_list:
                    attribute_list.append(attribute)
                numerical_attributes[point][attribute] = [data[point][attribute]]
                #numerical_attributes[attribute].append(data[point][attribute])
            else:
                categorial_attributes[point][attribute] = data[point][attribute]

    if evenly_distributed_steps == None:
        #print(numerical_attributes)
        normalization_coefficients = {}
        evenly_distributed_steps = {}
        for attribute in attribute_list:
            normalization_coefficients[attribute] = calc_normalization_coefficients(numerical_attributes, attribute)
            #print(normalization_coefficients[attribute])
            #step = normalization_coefficients[attribute]["extrema"]["power"][0]*3
            step = int(normalization_coefficients[attribute]["spans"][0]/intervals)
            min_val = int(normalization_coefficients[attribute]["extrema"]["min_values"][0])
            max_val = int(normalization_coefficients[attribute]["extrema"]["max_values"][0])
            #print(attribute, step, round_down(min_val, step), round_up(max_val, step))
            #evenly_distributed_steps[attribute] = list(range(round_down(min_val, step), round_up(max_val, step), step))[1:-1]
            evenly_distributed_steps[attribute] = list(range(min_val, max_val, step))[1:]
        #print(evenly_distributed_steps)

    new_data = categorial_attributes
    discrete_values = {}
    for point in numerical_attributes:
        discrete_values[point] = {}
        for attribute in numerical_attributes[point]:
            value = numerical_attributes[point][attribute][0]
            #print(numerical_attributes[point][attribute][0])
            step_name = None
            count = 1
            for step in evenly_distributed_steps[attribute]:
                if value <= step:
                    if step_name == None:
                        step_name = "<=" + str(step)
                    else:
                        #print("here")
                        step_name += "<=" + str(step)
                    break
                else:
                    #print(count,len(evenly_distributed_steps[attribute]))
                    if step_name == None:
                        step_name = ">" + str(step)
                    else:
                        step_name += ">" + str(step)
                    if count == len(evenly_distributed_steps[attribute]):
                        step_name = ">" + str(step)
                #print(attribute, value, step, step_name)
                count += 1
            new_data[point][attribute] = step_name

    #print(new_data)

    result = {
        "data":new_data,
        "steps":evenly_distributed_steps
    }

    return(result)

    # normalization_coefficients = calc_normalization_coefficients_list(numerical_attributes["age"])
    #print(normalization_coefficients)

def convert_to_data_points(lst):
    data = {}
    for dim in range (0,len(lst)):
        print(dim)
        count = 0

        for value in lst[dim]:
            count += 1
            name = "DP"+str(count)
            if name not in data:
                data[name] = [value]
            else:
                data[name].append(value)
            print(value)
    return data
                