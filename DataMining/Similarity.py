import DataMining.Functions
import math, pathlib

def Jaccard(data, X, Y, possible_options = None):
    """ returns the Jaccard Coefficient for two points of the data """
    q,r,s = 0,0,0
    #print(possible_options)
    if possible_options == None:
        possible_options = DataMining.Functions.determine_possible_options(data)
    for attribute in possible_options:
        #print(attribute,":")
        for value in possible_options[attribute]:
            #print ("    ",value)
            if (data[X][attribute] == value and data[Y][attribute] == value):
                q += 1
                #print ("    q:", data[X][attribute], data[Y][attribute])
            elif (data[X][attribute] == value and data[Y][attribute] != value):
                r += 1
                #print ("    r:", data[X][attribute], data[Y][attribute])
            elif (data[X][attribute] != value and data[Y][attribute] == value):
                s += 1
                #print ("    s:", data[X][attribute], data[Y][attribute])
                
    #print(q,r,s)
    return((r+s)/(q+r+s))


def IoF(data, X, Y, possible_options = None):
    """ returns the IoF for two points of the data """
    distance = 0
    if possible_options == None:
        possible_options = DataMining.Functions.determine_possible_options(data)
        all_occurences = DataMining.Functions.count_all_occurences(data, possible_options)
    else:
        all_occurences = DataMining.Functions.count_all_occurences(data)
    #print(option_probabilities)
    for attribute in possible_options:
        #print(attribute,":")
        #print (data[X][attribute], data[Y][attribute], \
        # data[X][attribute] != data[Y][attribute])
        if (data[X][attribute] != data[Y][attribute]):
            f_a = all_occurences[attribute][data[X][attribute]]
            f_b = all_occurences[attribute][data[Y][attribute]]
            #print(X, "-", Y, data[X][attribute], f_a, data[Y][attribute], f_b)
            distance += (1 - (1 / (1 + math.log(f_a, 10)* math.log(f_b, 10))))
    return round((1/len(possible_options)*distance),3)
    
def Cosine(X,Y):
    """ returns the Cosine Similarity for two points """
    numerator = 0
    X_denumerator = 0
    Y_denumerator = 0
    for attribute in X:
        numerator += (X[attribute] * Y[attribute])
        X_denumerator += (X[attribute]**2)
        Y_denumerator += (Y[attribute]**2)
    #print(numerator, X_denumerator, Y_denumerator, \
    #    math.sqrt(X_denumerator), sqrt(Y_denumerator))
    return round((numerator/(math.sqrt(X_denumerator) * math.sqrt(Y_denumerator))),3)

def count_word_occurence(filename, word):
    """ returns the count of a word in the text loaded from the loaction """
    counter = 0
    with open(filename, encoding="utf-8") as openfile:
        for line in openfile:
            for part in line.split():
                if word in part:
                    counter+=1
    openfile.close()
    return counter

def clean_text(filename, pathname = "texts"):
    """ returns the cleaned lowecase text with only text and numbers"""
    import re
    input_path = pathlib.Path(pathname, filename)
    #print("path to input file:", input_path)
    input_file = open(input_path, "r", encoding="utf-8")
    content = input_file.read()
    input_file.close()
    cleaned_content = ""
    for k in content.split("\n"):
        cleaned_content+=(re.sub(r"[^a-zA-ZäÄöÖüÜ0-9]+", ' ', k)).lower()+" "
    output_path = pathlib.Path(pathname, "cleaned_" + filename)
    output_file = open(output_path, "w", encoding="utf-8")
    output_file.write(cleaned_content)
    output_file.close()

def count_word_occurrences(filename, word_list, pathname = "texts"):
    """ returns the count of all words in the list from given path"""
    occurences = {}
    #path = pathname + filename + ".txt"
    path = pathlib.Path(pathname, "cleaned_" + filename)
    for search_word in word_list:
        occurences[search_word] = count_word_occurence(path,search_word)
    return occurences

def compare_texts(text_files, word_list = None, pathname = ""):
    """ returns the count of all words in the list from all documents """
    word_occurences = {}
    if word_list is not None:
        for text in text_files:
            clean_text(text, pathname = pathname)
            word_occurences[text] = count_word_occurrences(text, word_list , pathname = pathname)
    #print(json.dumps(word_occurences, indent=4, sort_keys=True))
    return word_occurences

