import DataMining.Functions
import itertools as it
import json, ast


def Calculate(data, min_sup = 2, min_conf = 70):

    itemsets = DataMining.Functions.determine_possible_options(data)
    #print("itemsets", itemsets)

    supports = {}
    calc_supports(data, itemsets, supports)
    #print("supports", supports)

    pruned_itemsets = prune_itemsets(itemsets, supports, min_sup)
    #print("pruned_itemsets", pruned_itemsets)

    frequent_itemsets = determine_frequent_itemsets(pruned_itemsets, supports, min_sup)
    #print("frequent_itemsets", frequent_itemsets)

    itemsets = (list(it.combinations(frequent_itemsets, 2)))
    last_frequent_itemsets = itemsets
    #print("last itemsets", itemsets)

    while True:
        calc_supports(data, itemsets, supports)
        #print("last supports", supports)
        pruned_itemsets = prune_itemsets(itemsets, supports, min_sup)
        #print("last pruned_itemsets", pruned_itemsets)
        frequent_itemsets = determine_frequent_itemsets(pruned_itemsets, supports, min_sup)
        if len(frequent_itemsets) == 0:
            #print("last frequent_itemsets", frequent_itemsets)
            break
        else:
            last_frequent_itemsets = frequent_itemsets
            #print("last frequent_itemsets", frequent_itemsets)
        itemsets = join_itemsets(frequent_itemsets)
        #print("last itemsets", itemsets)
    #print("itemsets:", itemsets)
        

    prototype_rules = create_prototype_rules(last_frequent_itemsets)
    confidences = calc_confidences(data, prototype_rules)
    #print(json.dumps(confidences, indent=2, sort_keys=True))
    selected_rules = select_rules(confidences, min_conf = min_conf)
    #print(json.dumps(selected_rules, indent=2, sort_keys=True))
    #print(selected_rules)
    return selected_rules

def join_itemsets(itemsets):
    joined_itemsets = []
    for n1 in range(0, len(itemsets)):
        #joined_itemsets.append()
        #print(itemsets[n1], list(it.combinations(itemsets[n1], 2)))
        for n2 in range(0, len(itemsets)):
            new_set = set(itemsets[n1]) | set(itemsets[n2])
            set_name = sort_set(new_set)
            if len(new_set) > len(itemsets[n1]) and len(new_set) < len(itemsets[n1]) + 2:
                if set_name not in joined_itemsets:
                    #print(new_set, new_set not in joined_itemsets)
                    joined_itemsets.append(set_name)
    return joined_itemsets

def determine_frequent_itemsets(itemsets, supports, min_sup):
    frequent_itemsets = []
    for itemset in itemsets:
        set_name = str(sort_set(itemset))
        #print(itemset, supports[set_name])
        if supports[set_name] >= min_sup:
            frequent_itemsets.append(itemset)
    return frequent_itemsets


def calc_supports(data, itemsets, supports):
    for itemset in itemsets:
        set_name = str(sort_set(itemset))
        if type(itemset) == str:
            itemset = set([itemset])
        #print("set_name", set_name)
        #print("#############################",type(itemset), itemset, set(itemset))
        if set_name not in supports:
            supports[set_name] = 0
        #print(set(itemset))
        for data_set in data:
            #print(set(data[data_set]), set(itemset).issubset(set(data[data_set])))
            if set(itemset).issubset(set(data[data_set])):
                supports[set_name] += 1            
    return 



def prune_itemsets(itemsets, supports, min_sup):
    #print("#############################")
    #print("supports:", supports)
    #print("len(itemsets)", len(itemsets))
    #print("itemsets", itemsets)
    pruned_itemsets = []
    for itemset_num in range(0, len(itemsets)):
        #print(itemset_num, itemsets[itemset_num])
        if type(itemsets[itemset_num]) == str:
            if supports[itemsets[itemset_num]] >= min_sup:
                    pruned_itemsets.append(itemsets[itemset_num])
        else:
            append = 1
            #print(itemsets[itemset_num], len(itemsets[itemset_num]))
            if len(itemsets[itemset_num]) > 2:
                subsets = sort_set((list(it.combinations(itemsets[itemset_num], 2))))
            else:
                subsets = itemsets[itemset_num]
            #print(subsets)
            for subset in subsets:
                set_name = str(sort_set(subset))
                #print("set_name", set_name)#, "supports[set_name]:", supports[set_name])
                if supports[set_name] < min_sup:
                    append = 0
                    break
            if append == 1:
                pruned_itemsets.append(itemsets[itemset_num])
    return pruned_itemsets


def sort_sets(itemsets):
    sorted_itemsets = []
    for itemset in itemsets:
        sorted_itemsets[itemset] = sort_set(itemset)
    return sorted_itemsets

def sort_set(itemset):
    #print(itemset, type(itemset))
    if type(itemset) == str:
        return itemset
    else:
        #for subset_num in range (0, len(itemset)):
        #print("old subset", itemset)
        sorted_itemset = list(itemset) #(list(subsets[subset_num])).sort()
        sorted_itemset.sort()
        #print("sorted_itemset", sorted_itemset)
        return sorted_itemset


def conv_to_itemset(data):
    itemset = []
    #print(data)
    for point in data:
        #print(point)
        try:       
            if type(data[point]) == list or type(data[point]) == tuple: 
                itemset.append(data[point])
        except:
            itemset.append(point)
    return itemset

def select_rules(confidences, min_conf = 70):
    selected_rules = []
    for rule in confidences:
        forward_confidence = confidences[str(rule)]["forward_numerator"] / confidences[str(rule)]["forward_denumerator"]
        confidences[str(rule)]["forward_confidence"] = forward_confidence
        if forward_confidence*100 >= min_conf:
            #rule = ast.literal_eval(rule)
            #print(rule, ":", forward_confidence*100, "%")
            selected_rules.append(ast.literal_eval(rule))
            
        backward_confidence = confidences[str(rule)]["backward_numerator"] / confidences[str(rule)]["backward_denumerator"]
        confidences[str(rule)]["backward_confidence"] = backward_confidence
        if backward_confidence*100 >= min_conf:
            rule = ast.literal_eval(rule)
            #print(rule, type(rule))
            #print(rule, ":", backward_confidence*100, "%")
            rotaded_rule = ast.literal_eval("(" + str(rule[1]) + "," + str(rule[0]) + ")")
            selected_rules.append(rotaded_rule)
    return selected_rules

def calc_confidences(data, prototype_rules):
    confidences = {}
    for transaction in data:
        #print(transaction)
        for rules in prototype_rules:
            #print(rules)
            for rule in prototype_rules[rules]:
                if str(rule) not in confidences:
                    confidences[str(rule)] = {
                        "forward_numerator": 0,
                        "forward_denumerator": 0,
                        "backward_numerator": 0,
                        "backward_denumerator": 0
                    }
                if set(rule[0]).issubset(set(data[transaction])):
                    confidences[str(rule)]["forward_denumerator"] +=1
                    if set(rule[1]).issubset(set(data[transaction])):
                        confidences[str(rule)]["forward_numerator"] +=1

                if set(rule[1]).issubset(set(data[transaction])):
                    confidences[str(rule)]["backward_denumerator"] +=1
                    if set(rule[0]).issubset(set(data[transaction])):
                        confidences[str(rule)]["backward_numerator"] +=1
                #print(rule[0], rule[1], set(rule[0]).issubset(set(data[transaction])), set(rule[1]).issubset(set(data[transaction])), confidences[str(rule)])
    #print(confidences)
    return confidences   


def create_prototype_rules(itemsets):
    prototype_rules = {}
    for itemset in itemsets:
        set_name = str(sort_set(itemset))
        prototype_rules[str(set_name)] = []
        #print(itemset)
        for item in itemset:
            #print(item, set(itemset), set([item]))
            rule = (set([item]), (set(itemset) - set([item])))
            #print(rule)
            if rule not in prototype_rules[set_name]:
                prototype_rules[set_name].append(rule)
    #print(prototype_rules)
    return prototype_rules
        