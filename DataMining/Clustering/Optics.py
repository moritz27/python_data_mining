import DataMining.Functions
import DataMining.Plotting
import DataMining.Clustering.Functions
import statistics

def Calculate(data, max_epsilon = None, min_pts = 3, distance_function = "Euklid", start_point = None, normalized = None):
    """ creates a grapical representation for finding a good value for epsilon  """
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    #print(normalized)
    if max_epsilon == None:
        max_epsilon = normalized["coefficients"]["mean_power"]
    #print(max_epsilon)
    norm_max_epsilon = DataMining.Functions.normalize_value(max_epsilon, normalize_coefficients = normalized["coefficients"])
    #print(norm_max_epsilon)
    # norm_max_epsilon = 0.2
    distances = DataMining.Functions.calc_all_distances(normalized["data"], distance_function = distance_function)
    print("calculated distances between all points")
    #print("distances:", distances)
    core_distances = calc_core_distances(normalized["data"], min_pts, distances, normalized["coefficients"])
    print("calculated core distances of each point")
    #print("core_distances:", core_distances)
    reachability_distances = calc_reachability_distances(normalized["data"], distances, core_distances, max_epsilon = norm_max_epsilon)
    print("calculated reachability distances between all points")
    #print("reachability_distances:", reachability_distances)
    DataMining.Plotting.Reachability_Chart(distances, reachability_distances, normalized["coefficients"], start_point = start_point)


def calc_core_distances(data, min_pts, distances, norm_data):
    """ returns the core distances of all data points"""
    core_distances = {}
    for p1 in data:
        epsilon = 1 / statistics.mean(norm_data["spans"])
        core_distances[p1] = None
        while core_distances[p1] == None:
            epsilon_points = []
            for p2 in data:
                if p1 != p2:
                    #if D[p2]["visited"] == 0:
                    #print(p1, p2, distances[p1][p2], distances[p1][p2] <= epsilon)
                    if distances[p1][p2] <= epsilon:
                        epsilon_points.append(p2)
            #print(p1, epsilon, len(epsilon_points)+1)
            if (len(epsilon_points) + 1) >= min_pts:
                core_distances[p1] = epsilon
                break
            else:
                epsilon += 0.01
    return core_distances

def calc_reachability_distances(data, distances, core_distances, max_epsilon = 4):
    """ returns  the reachability distances of all data points"""
    reachability_distances = {}
    for p1 in data:
        reachability_distances[p1] = {}
        for p2 in data:
                if p1 != p2: # and p2 not in reachability_distances:
                    if distances[p1][p2] <= max_epsilon:
                        #print(p1, p2, distances[p1][p2], core_distances[p1])
                        if distances[p1][p2] > core_distances[p1]:
                            reachability_distances[p1][p2] = distances[p1][p2]
                        else:
                            reachability_distances[p1][p2] = core_distances[p1]
    return reachability_distances

