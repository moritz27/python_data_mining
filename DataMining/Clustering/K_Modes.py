import DataMining.Functions
import DataMining.Distance
import DataMining.Clustering.Functions
import random

def Calculate(data, inital_modes = None, mode_num = 2, distance_function = "Hamming"):
    """ returns clusters of the given categorial data"""
    occurences = DataMining.Functions.count_all_occurences(data)
    #print(occurences)
    cat_centroids = choose_init_cat_centroids(data, mode_num)
    print("cat_centroids:", cat_centroids)
    cat_distances = calculate_cat_distances(data, cat_centroids, distance_function = distance_function)
    print("cat_distances:", cat_distances)
    clusters = DataMining.Clustering.Functions.assign_to_cluster(cat_distances)
    cleaned_clusters = DataMining.Clustering.Functions.clean_clusters(clusters)
    print("cleaned_clusters:", cleaned_clusters)
    #cluster_occurences = calc_cluster_occurences(data, clusters)
    previoius_clusters = None
    while cleaned_clusters != previoius_clusters:
        previoius_clusters = cleaned_clusters
        clusters_modes = find_clusters_modes(data, cleaned_clusters)
        #data.update(clusters_modes)
        print("clusters_modes:", clusters_modes)
        cat_distances = calculate_cat_distances(data, clusters_modes, distance_function = distance_function)
        print(cat_distances)
        clusters = DataMining.Clustering.Functions.assign_to_cluster(cat_distances)
        cleaned_clusters = DataMining.Clustering.Functions.clean_clusters(clusters)
        print("cleaned_clusters:", cleaned_clusters)
    return clusters

def find_clusters_modes(data, clusters):
    modes = {}
    for c in clusters:
        cluster_points = {}
        for p in clusters[c]:
            #print(p, "is in", c)
            cluster_points[p] = data[p]
        occurences = DataMining.Functions.count_all_occurences(cluster_points)
        print(occurences)
        modes[c] = determine_modes(occurences)
    return modes


def choose_init_cat_centroids(data, centroid_num):
    """ returns random centroids"""
    inital_centroids = {}
    #for i in range(0, centroid_num):
    while len(inital_centroids) < centroid_num:
        entry_list = list(data.items())
        random_entry = random.choice(entry_list)
        #print(random_entry[1])
        if random_entry[0] not in inital_centroids:
            inital_centroids[random_entry[0]] = random_entry[1]
    return inital_centroids
    
def calculate_cat_distances(data, centroids, distance_function = "Hamming"):
    """ returns  the distances between all data points and all centroids of the clusters"""
    distances = {}
    for point in data:
        #print(point)
        distances[point] = {}
        for r in centroids:
            #print(r)
            method_to_call = getattr(DataMining.Distance, distance_function)
            if r.startswith("C"):
                distances[point][r] = method_to_call(data, point, centroids[r])
            else:
                distances[point][r] = method_to_call(data, point, r)
    return distances

def calc_cluster_occurences(data, clusters):
    occurences = {}
    for point in data:
        for cl in clusters:
            print(point, "is in", cl)

def determine_modes(occurences):
    """ returns the most occuring categorial value for each attribute"""
    modes = {}
    for attribute in occurences:
        modes[attribute] = {}
        mode = ""
        count = 0
        for value in occurences[attribute]:
            if occurences[attribute][value] > count:
                mode = value
                count = occurences[attribute][value]
                #modes[attribute]["count"] = occurences[attribute][value]
        #print(mode, count)
        #modes[attribute][mode] = occurences[attribute][mode]
        modes[attribute] = mode
    return modes