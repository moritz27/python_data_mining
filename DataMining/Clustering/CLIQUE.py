import DataMining.Functions
import DataMining.Clustering.Functions
import random, math
import itertools as it

def Calculate(data, min_pts = 5, normalized = None, cell_step_sizes = None):
    """ returns clusters of the given data based on min_pts"""
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    
    
    cell_sizes = [[] for i in range(dimensions)]
    for dimension in range (0,dimensions):
        min_val = normalized["coefficients"]["extrema"]["min_values"][dimension]
        max_val = normalized["coefficients"]["extrema"]["max_values"][dimension]
        if cell_step_sizes == None:
            step =  normalized["coefficients"]["extrema"]["power"][dimension]
        else:
            step = cell_step_sizes[dimension]
            print(step)
        cell_sizes[dimension] = list(range(DataMining.Functions.round_down(min_val, step), DataMining.Functions.round_up(max_val, step), step))

    print(cell_sizes)

    projections = [[] for i in range(dimensions)]
    assignment = {}
    for point in data:
        #print(data[point])
        projection_list = []
        for dimension in range(0,dimensions):
            for cell_size_num in range(1, len(cell_sizes[dimension])):
                name = str(cell_sizes[dimension][cell_size_num-1]) +  "-" + str(cell_sizes[dimension][cell_size_num])
                if name not in projections[dimension]:
                    projections[dimension].append(name)
                if data[point][dimension] <= cell_sizes[dimension][cell_size_num]:
                    projection_list.append(name)
                    break
        assignment[point] = tuple(projection_list)

    print(assignment)
    print(projections)

    all_projections = list(it.product(*projections))

    cells = {}
    for pro in all_projections:
        #print(pro)
        cells[str(pro)] = []
    

    for point in assignment:
        #print(assignment[point])
        if str(assignment[point]) in cells:
            cells[str(assignment[point])].append(point)
   
    #print(cells)

    clusters = {}

    for c in cells:
        if len(cells[c]) >= min_pts:
            clusters[c] = cells[c]

    #print(json.dumps(clusters, indent=2, sort_keys=True))
    return (clusters)

