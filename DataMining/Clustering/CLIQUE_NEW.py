import DataMining.Functions
import random, math

def Calculate(data, min_pts = 5, normalized = None):
    """ returns clusters of the given data based on min_pts"""
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])

    #print(dimensions)
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    
    cell_limits = [[] for i in range(dimensions)]
    for dimension in range (0,dimensions):
        min = normalized["coefficients"]["extrema"]["min_values"][dimension]
        max = normalized["coefficients"]["extrema"]["max_values"][dimension]
        step =  normalized["coefficients"]["extrema"]["power"][dimension]
        cell_limits[dimension] = list(range(round_down(min, step), round_up(max, step), step))
    #print(cell_limits)

    lenghts = []
    for i in range (0, len(cell_limits)):
        lenghts.append(len(cell_limits[i])-1)

    print("lenghts", lenghts)
    
    cells = create_n_dimensional_list(lenghts)

    #print(cells)

    for point in data:
        assigned_cell = []
        for dimension in range(0,dimensions):
            for cell_num in range(0, len(cell_limits[dimension])):
                if data[point][dimension] <= cell_limits[dimension][cell_num+1]:
                    #print(data[point][dimension], "<=", cell_limits[dimension][cell_num])
                    assigned_cell.append(cell_num)
                    break
        #print(point, data[point], assigned_cell)
        #cells[assigned_cell[0]][assigned_cell[1]].append(point)
        # target = read_by_index(cells, assigned_cell[:-1])
        # target[assigned_cell[-1]].append(point)
        cells = DataMining.Functions.write_by_index(cells, point, append = True, indexes=assigned_cell)
        

    print(cells)

    clusters = []

    #iterate_over_n_dimensional_list(cells)

    for X in range(0,len(cells)):
        for Y in range(0, len(cells[X])):
            print(X, Y, cells[X][Y])
            if len(cells[X][Y]) >= min_pts:
                print(check_if_neighbor_cells_are_dense(cells, [X,Y], min_pts)) 


def check_if_neighbor_cells_are_dense(cells, cell_coords, min_pts):
    """ returns the density of the neighbors of a given cell"""
    density_list = []
    try:
        if len(cells) > cell_coords[0]+1:
            if len(cells[cell_coords[0]+1][cell_coords[1]]) >= min_pts:
                density_list.append(1)
            else:
                density_list.append(0)
        else:
            density_list.append(0)
    except:
        density_list.append(0)

    try:
        if cell_coords[1]-1 > 0:
            if len(cells[cell_coords[0]][cell_coords[1]-1]) >= min_pts:
                density_list.append(1)
            else:
                density_list.append(0)
        else:
            density_list.append(0)
    except:
        density_list.append(0)

    try:
        if cell_coords[0]-1 > 0:
            if len(cells[cell_coords[0]-1][cell_coords[1]]) >= min_pts:
                density_list.append(1)
            else:
                density_list.append(0)
        else:
            density_list.append(0)
    except:
        density_list.append(0)

    try:
        if len(cells[1]) > cell_coords[1]+1:
            if len(cells[cell_coords[0]][cell_coords[1]+1]) >= min_pts:
                density_list.append(1)
            else:
                density_list.append(0)
        else:
            density_list.append(0)
    except:
        density_list.append(0)

    return density_list

def round_up(num, factor):
    return int(math.ceil(num / factor)*factor) + factor

def round_down(num, factor):
    return int(math.floor(num / factor)*factor)


def flat(l):
    depths = []
    for item in l:
        if isinstance(item, list):
            depths.append(flat(item))
    if len(depths) > 0:
        return 1 + max(depths)
    return 1

def create_n_dimensional_list(lenghts):
    """ returns a list with any wanted dimensions"""
    lst = [[] for i in range(lenghts[0])]
    #print("in function",lst,  n, lenghts)
    if len(lenghts) > 1:
        for j in range(0, len(lst)):
            #print("j:", j, lenghts[1:])
            lst[j] = create_n_dimensional_list(lenghts[1:])
    return lst

def iterate_over_n_dimensional_list(lst, function = None, top_index = None):
    """ iterates over a list with any dimensions and applies a function"""
    if top_index == None:
        top_index = [0]
    #print("index", top_index)
    for element_num in range(0, len(lst)):
        if type(lst[element_num]) != str :
            top_index[-1] = element_num
            print(element_num, top_index)
            iterate_over_n_dimensional_list(lst[element_num], function = function, top_index = top_index)
           
        else:
            print(lst[element_num])
            if function != None:
                pass
    return 
