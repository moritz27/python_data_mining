import random, math, collections
import DataMining.Distance

def merge_clusters(clusters, neighbours):
    """ combines the neighbours into a new cluster """
    new_cluster = []
    for i in range (0, len(neighbours)):
        new_cluster.extend(clusters[neighbours[i]])
        del clusters[neighbours[i]]
    i = 1
    while True:
        cluster_name = "C"+str(i)
        #print(cluster_name, cluster_name not in clusters)
        if cluster_name not in clusters:
            #print(cluster_name)
            clusters[cluster_name] = new_cluster
            break
        i+=1
    #print(clusters)
    return #clusters

def create_cluster_for_each_point(data):
    """ returns a dict of clusters each containing only point """
    clusters = {}
    cluster_counter = 1
    for point in data:
        clusters["C"+ str(cluster_counter)] = [point]
        cluster_counter += 1
    return clusters

def clean_clusters(clusters, min_points = 0):
    """ returns a list of clusters with continous numbering """
    clean_clusters = {}
    cluster_counter = 1
    for c in clusters:
        if len(clusters[c]) > min_points:
            clean_clusters["C"+str(cluster_counter)] = clusters[c]
            cluster_counter += 1
    return clean_clusters

def sum_of_squared_errors(data, clusters):
    """ returns the sum of the squared errors for the clusters """
    sum = 0
    for c in clusters:
        rand_key = random.choice(list(data.keys()))
        dimensions = len(data[rand_key])
        means = calc_mean_of_cluster(data, clusters[c], dimensions)#statistics.mean(clusters[c])
        for p in clusters[c]:
            #print(p, data[p])
            for s in range(0, dimensions):
                sum += (data[p][s] - means[s])**2
    return round(sum,2) 

def calc_mean_of_cluster(data, cluster, dimensions):
    """ returns the mean values for the given clusters"""
    sums = dimensions*[0]
    #print("c:",c, "sums:", sums)
    for point in cluster:
        #print("point:", point,"data[point]:", data[point])
        for d in range(0, dimensions):
            #print(d, sums, data[point][d])
            sums[d] += data[point][d]
        #print(point, data[point])
    means = []
    for s in range(0, dimensions):
        means.append(sums[s]/len(cluster))
    #print(sums,len(clusters[c]), means)
    #new_centroids.append(statistics.mean(clusters[c]))
    return means

def calculate_num_distances(data, centroids, distance_function = "Cityblock", distances = None):
    """ returns the distances between all points in data and the centroids"""
    #print(data, centroids)
    if distances == None:
        distances = {}
    for point in data:
        #print(point)
        if point not in distances:
            distances[point] = {}
        for ct in range(0, len(centroids)):
            #print(point, data[point], ct)
            #distances[point]["C"+ str(ct+1)] = globals()["DataMining.Distance."+distance_function](data[point], centroids[ct])
            method_to_call = getattr(DataMining.Distance, distance_function)
            distances[point]["C"+ str(ct+1)] = method_to_call(data[point], centroids[ct])
    return distances

    #distance = globals()[distance_function]()
    #for i in 

def assign_to_cluster(distances):
    """ returns a list of clusters based on the given distances """
    #print(distances)
    clusters = {}
    for point in distances:
        #print("#################################################")
        #print(point)
        #assigned_cluster = random.choice(distances[point].keys()) 
        assigned_cluster = random.choice(list(distances[point].keys()))
        cluster_name = "C"
        #print("assigned_cluster:", assigned_cluster, "assigned_cluster_distance:", distances[point][assigned_cluster])
        for distance in distances[point]:
            #print("point:", point, "with distance to", distance, "of:", distances[point][distance])
            if distances[point][distance] < distances[point][assigned_cluster]:
                assigned_cluster = distance
                #print(assigned_cluster)
        if assigned_cluster not in clusters:
            clusters[assigned_cluster] = []
        clusters[assigned_cluster].append(point)
    return collections.OrderedDict(sorted(clusters.items()))