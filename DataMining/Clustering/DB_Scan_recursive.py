import DataMining.Functions
import DataMining.Clustering.Functions
import statistics, random, json

def Calculate(data, epsilon = 3, min_pts = 3, distance_function = "Euklid", normalized = None):
    """ returns clusters of the given data based on epsilon and min_points"""
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    norm_epsilon = epsilon / statistics.mean(normalized["coefficients"]["spans"])  
    #print(norm_epsilon)
    distances = DataMining.Functions.calc_all_distances(normalized["data"], distance_function = distance_function)
    point_status = {}
    for point in normalized["data"]:
        point_status[point] = {}
        point_status[point]["visited"] = 0
        point_status[point]["type"] = None
        point_status[point]["cluster"] = None
    clusters = {}
    for point in point_status:
        if point_status[point]["visited"] == 0: 
            check_surrounding(data, distances, point, norm_epsilon, min_pts, point_status, clusters)
    #print(json.dumps(point_status, indent=2, sort_keys=True))
    return DataMining.Clustering.Functions.clean_clusters(clusters)

def check_surrounding(data, distances, point, norm_epsilon, min_pts, point_status, clusters, top_cluster_name = None):
    abs_points = []
    if point_status[point]["visited"] == 0:
        point_status[point]["visited"] = 1
        for point_to_test in data:
            if point_to_test != point:
                if distances[point][point_to_test] <= norm_epsilon:
                    abs_points.append(point_to_test)
        #print(point, abs_points)
        if len(abs_points)+1 >= min_pts:
            point_status[point]["type"] = "core"
            if point_status[point]["cluster"] == None:
                if top_cluster_name == None:
                    cluster_name = "C"+str(len(clusters)+1)
                    point_status[point]["cluster"] = cluster_name
                    clusters[cluster_name] = [point]
                else:
                    point_status[point]["cluster"] = top_cluster_name
                    cluster_name = top_cluster_name
                    clusters[cluster_name].append(point)
            for new_point_to_test in abs_points:
                check_surrounding(data, distances, new_point_to_test, norm_epsilon, min_pts, point_status, clusters, cluster_name)
        else:
            if top_cluster_name == None:
                point_status[point]["type"] = "noise"
            else:
                point_status[point]["type"] = "edge"
                point_status[point]["cluster"] = top_cluster_name
                clusters[top_cluster_name].append(point)
                
    

    