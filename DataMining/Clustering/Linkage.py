import DataMining.Functions
import DataMining.Clustering.Functions

def Calculate(data, max_distance = None, mode="single", distance_function = "Euklid", normalized = None, round_value = None, min_points_for_clusters = 0):
    """ returns a unknown number of clusters based on the mean distance between the points"""
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data, round_value = round_value)
    #print(normalized)
    if max_distance == None:
        max_distance = normalized["coefficients"]["mean_span"] / 5
        normalized_max_distance = DataMining.Functions.normalize_value(max_distance, normalize_coefficients = normalized["coefficients"])
    else:
        normalized_max_distance = DataMining.Functions.normalize_value(max_distance, normalize_coefficients = normalized["coefficients"])
        normalized_max_distance += normalized_max_distance/1000000
    #print(normalized_max_distance, DataMining.Functions.normalize_value(normalized_max_distance, normalize_coefficients = normalized["coefficients"], backwards = True))
    clusters = DataMining.Clustering.Functions.create_cluster_for_each_point(data)
    link_type="nearest"
    if mode == "complete":    
        link_type="farthest"
    elif mode == "mean":
        link_type="mean"
    while 1:
        best_neighbours = find_best_neighbours(normalized["data"], clusters, normalized_max_distance, link_type, distance_function, normalized, round_value)
        if best_neighbours == 0:
            break
        #print(best_neighbours)
        DataMining.Clustering.Functions.merge_clusters(clusters, best_neighbours)
    return DataMining.Clustering.Functions.clean_clusters(clusters, min_points = min_points_for_clusters)
    

def find_best_neighbours(data, clusters, max_distance, link_type="nearest", distance_function = "Euklid", normalized = None, round_value = None):
    """ returns the two best neigbhbours based on the lowest (mean) or highest distance between the points of the clusters"""
    best_neigbours = {
        "nearest":
            {
                "clusters" :  None,
                "distance" : None
            },
        "mean":
            {
                "clusters" :  None,
                "distance" : None
            },
        "farthest":
            {
                "clusters" :  None,
                "distance" : None
            }
    }

    for c1 in clusters: 
        for c2 in clusters:
            if c2 != c1:
                distance_sum = 0
                biggst_distance = None
                for p1 in clusters[c1]:
                    for p2 in clusters[c2]:
                        if p1 != p2:
                            method_to_call = getattr(DataMining.Distance, distance_function)
                            distance = method_to_call(data[p1], data[p2], round_value)
                            if best_neigbours["nearest"]["distance"] == None or distance < best_neigbours["nearest"]["distance"]:
                                best_neigbours["nearest"]["distance"] = distance
                                best_neigbours["nearest"]["clusters"] = [c1,c2]
                            if biggst_distance == None or distance > biggst_distance:
                                biggst_distance = distance
                                biggst_distance_clusters = [c1,c2]
                            distance_sum += distance
                if best_neigbours["farthest"]["distance"] == None or biggst_distance < best_neigbours["farthest"]["distance"]:
                        best_neigbours["farthest"]["distance"] = biggst_distance
                        best_neigbours["farthest"]["clusters"] = biggst_distance_clusters

                mean_distance = distance_sum / (len(clusters[c1]) * len(clusters[c2]))

                if best_neigbours["mean"]["distance"] == None or mean_distance < best_neigbours["mean"]["distance"]:
                        best_neigbours["mean"]["distance"] = mean_distance
                        best_neigbours["mean"]["clusters"] = [c1,c2]

    if best_neigbours[link_type]["distance"] == None or best_neigbours[link_type]["distance"] > max_distance:
        return 0
    else:
        return best_neigbours[link_type]["clusters"]
    

 