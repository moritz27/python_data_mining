import DataMining.Functions
import DataMining.Similarity
import DataMining.Clustering.Functions


def Calculate(data, threshold = 0.5, similarity_function = "Jaccard"):
    possible_options = DataMining.Functions.determine_possible_options(data)
    #print(possible_options)
    clusters = DataMining.Clustering.Functions.create_cluster_for_each_point(data)
    #print(clusters)
    pass_num = 1
    while True:
        print("Pass Num =", pass_num)
        pass_num += 1
        link_nums = calc_all_links(data, clusters, threshold, possible_options)
        #print(link_nums)
        best_clusters = find_best_clusters_to_merge(link_nums)
        #print(best_clusters)
        if best_clusters == None:
            break
        DataMining.Clustering.Functions.merge_clusters(clusters, best_clusters)
        print(clusters)
    return DataMining.Clustering.Functions.clean_clusters(clusters, min_points = 1)

def calc_all_links(data, clusters, threshold, possible_options):
    link_nums ={}
    for c1 in clusters:
        if c1 not in link_nums:
            link_nums[c1] = {}
        for c2 in clusters:
            if c1 != c2:
                if c2 not in link_nums[c1]:
                    link_nums[c1][c2] = 0
                for p1 in clusters[c1]:
                    for p2 in clusters[c2]:
                        similarity = DataMining.Similarity.Jaccard(data, p1, p2, possible_options)
                        if similarity <= threshold:
                            link_nums[c1][c2] += 1
                        #print(p1, p2 , similarity)
    return link_nums

def find_best_clusters_to_merge(link_nums):
    best_clusters = None
    highest_link_num = 0
    for cluster in link_nums:
        highest_link_num_cluster = DataMining.Functions.key_of_max_value(link_nums[cluster])
        if link_nums[cluster][highest_link_num_cluster] > highest_link_num:
            highest_link_num = link_nums[cluster][highest_link_num_cluster]
            best_clusters = [cluster, highest_link_num_cluster]
        #print(highest_link_num_cluster)
    return best_clusters



