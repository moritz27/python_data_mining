import DataMining.Functions
import DataMining.Clustering.Functions
import random, math
import itertools as it

def Calculate(data, n = None, normalized = None, cell_step_sizes = None, distance_function = "Euklid"):
    """ returns clusters of the given data based on min_pts"""
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    
    if n == None:
        n = int(len(data) / 10)
        print(n)
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)

    picked_points = DataMining.Functions.pick_n_points(data, n)
    print(picked_points)

    #distances = DataMining.Functions.calc_all_distances(normalized["data"], distance_function)
    distances = DataMining.Functions.calc_all_distances(data, distance_function = distance_function)
    #print(distances)
    
    evenly_distributed_points = DataMining.Functions.create_evenly_distributed_points(data, normalized, cell_step_sizes)
    #print(evenly_distributed_points)

    #normalized_evenly_distributed_points = DataMining.Functions.create_evenly_distributed_points(normalized["data"], normalized, cell_step_sizes, normalized_return = True)
    #print(normalized_evenly_distributed_points)

    
    distances_to_evenly_distributed_points = DataMining.Functions.calc_all_distances(data, evenly_distributed_points, distance_function)
    #distances_to_evenly_distributed_points = DataMining.Functions.calc_all_distances(normalized["data"], normalized_evenly_distributed_points, distance_function)

    #print(distances_to_evenly_distributed_points)


    sum_of_min_squared_distances_to_points = 0
    sum_of_min_squared_distances_to_evenly_distributed_points = 0
    for point in picked_points:
        #print(distances[point])
        point_min_key = DataMining.Functions.key_of_min_value(distances[point])
        point_min_value = distances[point][point_min_key]
        #print(point_min_key, point_min_value)
        sum_of_min_squared_distances_to_points += point_min_value**dimensions

    for point in data:
        evenly_distributed_point_min_key = DataMining.Functions.key_of_min_value(distances_to_evenly_distributed_points[point])
        evenly_distributed_point_min_value = distances_to_evenly_distributed_points[point][evenly_distributed_point_min_key]
        #print(evenly_distributed_point_min_key, evenly_distributed_point_min_value)
        sum_of_min_squared_distances_to_evenly_distributed_points += evenly_distributed_point_min_value**dimensions

        hopkins_statistic = sum_of_min_squared_distances_to_evenly_distributed_points / (sum_of_min_squared_distances_to_evenly_distributed_points + sum_of_min_squared_distances_to_points)
        #hopkins_statistic = sum_of_min_squared_distances_to_points / (sum_of_min_squared_distances_to_evenly_distributed_points + sum_of_min_squared_distances_to_points)

    return hopkins_statistic, evenly_distributed_points
    