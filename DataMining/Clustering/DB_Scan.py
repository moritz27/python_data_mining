import DataMining.Functions
import DataMining.Clustering.Functions
import statistics

def Calculate(data, epsilon = 3, min_pts = 3, distance_function = "Euklid", normalized = None):
    """ returns clusters of the given data based on epsilon and min_points"""
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    norm_epsilon = epsilon / statistics.mean(normalized["coefficients"]["spans"])  
    distances = DataMining.Functions.calc_all_distances(normalized["data"], distance_function = distance_function)
    D = {}
    for point in normalized["data"]:
        D[point] = {}
        D[point]["visited"] = 0
        D[point]["type"] = None
        D[point]["Cluster"] = None
    #print(D)
    clusters = {}
    cluster_counter = 1
    for p1 in D:
        if D[p1]["visited"] == 0:
            D[p1]["visited"] = 1
            D[p1]["abs_points"] = []
            for p2 in normalized["data"]:
                if p1 != p2:
                    #if D[p2]["visited"] == 0:
                    #print(p1, p2, distances[p1][p2], distances[p1][p2] <= norm_epsilon)
                    if distances[p1][p2] <= norm_epsilon:
                        D[p1]["abs_points"].append(p2)
            if (len(D[p1]["abs_points"]) + 1) >= min_pts:
                D[p1]["type"] = "core"
                #print(p1, "is a core object")
                if D[p2]["Cluster"] == None:
                    cluster_name = "C"+str(cluster_counter)
                    clusters[cluster_name] = []
                    cluster_counter += 1
                else:
                    cluster_name = D[p2]["Cluster"]
                
                #print(p1, D[p1]["abs_points"], cluster_name)
                for p3 in D[p1]["abs_points"]:
                    if D[p3]["Cluster"] == None:
                        clusters[cluster_name].append(p3)
                        D[p3]["Cluster"] = cluster_name
                    else:
                        cluster_name = D[p3]["Cluster"]
                    D[p3]["visited"] = 1
                    D[p3]["abs_points"] = []
                    for p4 in normalized["data"]:
                        if p3 != p4:
                            #print(p3, p4, distances[p3][p4], distances[p3][p4] <= norm_epsilon)
                            if distances[p3][p4] <= norm_epsilon:
                                D[p3]["abs_points"].append(p4)
                                if D[p3]["Cluster"] == None:
                                    clusters[cluster_name].append(p4)
                                    D[p4]["Cluster"] = cluster_name
                                else:
                                    cluster_name = D[p3]["Cluster"]
                    if (len(D[p3]["abs_points"]) + 1) >= min_pts:
                        D[p3]["type"] = "core"
                        #print(p3, "is a core object")
                    else:
                        D[p3]["type"] = "edge"
                        #print(p3, "is edge object")
                    #print(p3, D[p3]["abs_points"])
                clusters[cluster_name].append(p1)
                D[p1]["Cluster"] = cluster_name
                
            else:
                D[p1]["type"] = "noise"
                #print(p1, "is noise")
        #print(p1, "is", D[p1]["type"])
    
    #print(json.dumps(D, indent=4, sort_keys=True))
    return DataMining.Clustering.Functions.clean_clusters(clusters)