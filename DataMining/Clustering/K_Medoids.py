import DataMining.Functions
import DataMining.Clustering.Functions
import DataMining.Distance
import itertools as it

def Calculate(data, medoid_num = 2, distance_function = "Euklid", normalized = None):
    """ returns medoid_num clusters for the data """
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    #print(norm_data)
    best_error = None
    best_clusters = None
    best_medoids = None
    possible_combinations = calc_possible_combinations(normalized["data"], medoid_num)
    possible_points = combinations_to_points(normalized["data"], possible_combinations)
    #print(possible_points)
    for points in possible_points:
        #print(type(medoids))
        distances = DataMining.Clustering.Functions.calculate_num_distances(normalized["data"], points, distance_function = distance_function)
        clusters = DataMining.Clustering.Functions.assign_to_cluster(distances)
        error = DataMining.Clustering.Functions.sum_of_squared_errors(normalized["data"], clusters)
        if best_error == None:
            best_error = error
            best_clusters = clusters
            best_medoids = points
        #print(clusters, error, best_error, error <= best_error)
        if error < best_error:
            #print("found better cluster than", best_error, "with value of", error, )
            best_error = error
            best_clusters = clusters
            best_medoids = points

    results = {
        "clusters" : DataMining.Clustering.Functions.clean_clusters(best_clusters),
        "best_medoids": DataMining.Functions.normalize_data(best_medoids, normalize_coefficients = normalized["coefficients"], backwards = True)["data"],
        "best_error": best_error
    }
    return results

def calc_possible_combinations(data, medoid_num):
    """ returns all possible combinations in the data """
    data_list = list(data.keys())
    possible_combinations = list(it.combinations(data_list, medoid_num))
    return possible_combinations

def calc_new_medoids(clusters, was_medoid):
    """ returns the new medoids for the clusters"""
    new_medoids = []  
    for i in range(0, len(clusters)):
        items = list(clusters.items())
        index = int(len(items[i][1])/2)
        #print(items[i][1][index])
        if items[i][1][index] not in was_medoid[i]:
            new_medoids.append(items[i][1][index])
    return new_medoids

def combinations_to_points(data, combinations):
    """ returns a list of points for the given combinations"""
    points = []
    #print(combinations)
    for comb in range (0, len(combinations)):
        #print(comb, combinations[comb])
        points.append([])
        for dimension in range (0, len(combinations[comb])):
            #print(comb, combinations[comb][dimension], data[combinations[comb][dimension]])
            points[comb].append(data[combinations[comb][dimension]])
    return points