import DataMining.Functions
import DataMining.Clustering.Functions
import random, collections

def Calculate(data, inital_centroids = None, centroid_num = 2, distance_function = "Euklid", normalized = None):
    """ returns centroid_num clusters for the data """
    print(data)
    if normalized == None:
        normalized = DataMining.Functions.normalize_data(data)
    print(normalized)
    if inital_centroids == None:
        inital_centroids = init_num_points(normalized["data"], centroid_num)
    #print("inital_centroids", inital_centroids)
    inital_distances = DataMining.Clustering.Functions.calculate_num_distances(normalized["data"], inital_centroids, distance_function = distance_function)
    #print("inital_distances", inital_distances)
    clusters = DataMining.Clustering.Functions.assign_to_cluster(inital_distances)
    #print("clusters", clusters)
    previoius_clusters = None
    while clusters != previoius_clusters:
        previoius_clusters = clusters
        centroids = find_new_centroids(normalized["data"], clusters)
        #print("centroids", centroids)
        distances = DataMining.Clustering.Functions.calculate_num_distances(normalized["data"], centroids, distance_function = distance_function)
        clusters = DataMining.Clustering.Functions.assign_to_cluster(distances)
        error = DataMining.Clustering.Functions.sum_of_squared_errors(normalized["data"], clusters)
        #print("clusters", clusters, "error", error)
    for cl in clusters:
        clusters[cl].sort()
    results = {
        "clusters" : DataMining.Clustering.Functions.clean_clusters(clusters),
        "centroids": DataMining.Functions.normalize_data(centroids, normalize_coefficients = normalized["coefficients"], backwards = True)["data"],
        "error": error
    }
    return results # collections.OrderedDict(sorted(clusters.items()))

def init_num_points(data, num, exclude = None):
    """ returns num random points frm the given data """
    rand_points = [] 
    while len(rand_points) < num:
        random_point = random.choice(list(data.keys()))
        if data[random_point] not in rand_points:
            if exclude != None:
                if random_point not in exclude:
                    rand_points.append(data[random_point])
            else:
                rand_points.append(data[random_point])
    return rand_points
       
def find_new_centroids(data, clusters):
    """ returns the new centroids for the clusters"""
    new_centroids = []
    for c in clusters:
        new_centroids.append(DataMining.Functions.calc_mean_of_data(data, clusters[c]))
    return new_centroids



    
