import DataMining.Functions
import statistics, math

def Calculate(X,Y):
    if len(X) == len(Y):
        mean_x = statistics.mean(X)
        mean_y = statistics.mean(Y)
        #print(mean_x, mean_y)
        numerator = 0
        denumerator_x = 0
        denumerator_y = 0       
        for p1, p2 in zip(X,Y):
            #print(p1,p2)
            denumerator_x += (p1-mean_x)**2
            denumerator_y += (p2-mean_y)**2
            numerator += (p1-mean_x)*(p2-mean_y)
        #print(numerator, denumerator_x, denumerator_y)
        denumerator_x = math.sqrt(denumerator_x)
        denumerator_y = math.sqrt(denumerator_y)
        #print(numerator, denumerator_x, denumerator_y)
        correlation = numerator / (denumerator_x * denumerator_y)
        return round(correlation,4)
    else:
        return 1