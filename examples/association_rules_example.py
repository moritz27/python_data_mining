import os, sys
from beautifultable import BeautifulTable
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import DataMining

data = {
    "0000023":("A","B","C","D","I","P"),
    "0000042":("A","D","E","F"),
    "0001337": ("B","C","D","E"),
    "0031415":("A","B","C","D","K"),
    "0112358":("A","C","G","T")
}



#data = DataMining.Functions.read_json_from_file("examples\\test_data\\association_rules.json")
#print(DataMining.Functions.convert_to_tuples(data))

result = DataMining.Association_Rules.Calculate(data, min_sup = 3, min_conf = 80)
print(result)