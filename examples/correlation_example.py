import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import DataMining

X = [100,3,57,17,21,98]
Y = [70,0,24,7,7,83]

results = DataMining.Correlation.Calculate(X,Y)
print("the correlation of X and Y is:", results)