import os, sys
from beautifultable import BeautifulTable
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import DataMining
import DataMining.Functions

table = BeautifulTable()
data_table = BeautifulTable()

data = DataMining.Functions.read_json_from_file("examples\\test_data\\mushroom.json")

DataMining.Functions.print_data_in_table(data)

possible_options = DataMining.Functions.determine_possible_options(data)

compare_pairs = [["DP1","DP2"],["DP4","DP5"],["DP7","DP8"],["DP3","DP5"]]

print("Evaluation:")
table.rows.append(["Datapoints", \
                   "Jaccard Coeff", \
                   "Hamming Distance", \
                   "IOF"])

for pair in compare_pairs:
    table.rows.append([pair[0]+"-"+pair[1], \
                       DataMining.Similarity.Jaccard(data, \
                                           pair[0],pair[1],\
                                           possible_options = \
                                               possible_options),  \
                       DataMining.Distance.Hamming(data,  \
                                    pair[0], pair[1]), \
                       DataMining.Similarity.IoF(data,  \
                                                    pair[0],pair[1], \
                                                    possible_options =  \
                                                        possible_options)])
    
print(table)

