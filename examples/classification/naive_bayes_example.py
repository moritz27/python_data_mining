import os, sys, json
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

skiing = DataMining.Functions.read_json_from_file("examples\\test_data\\skiing.json")
computer_buyers = DataMining.Functions.read_json_from_file("examples\\test_data\\computer_buyers.json")
tennis = DataMining.Functions.read_json_from_file("examples\\test_data\\tennis.json")

DataMining.Functions.print_data_in_table(skiing)

points_for_classification = {
        "A": {
            "weather":"sunny",
            "snowhight":">=50"
        },
        "B": {
            "weather":"rainy",
            "snowhight":"<50"
        },
        "C": {
            "weather":"snowy",
            "snowhight":"<50"
        }
    }

# points_for_classification = {
#     "X": {
#         "age":"youth",
#         "income":"medium",
#         "student":"yes",
#         "credit_rating":"fair"
#     }
# }

# points_for_classification = {
#     "A": {
#         "outlook":"sunny",
#         "temperature":"hot",
#         "humidity":"high",
#         "wind":"weak"
#     }    
# }

for point in points_for_classification:
    print(point, DataMining.Classification.Naive_Bayes.Calculate(skiing, points_for_classification[point]))