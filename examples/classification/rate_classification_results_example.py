import os, sys, json
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

classification_results = DataMining.Functions.read_json_from_file("examples\\test_data\\classification_results.json")

DataMining.Functions.print_data_in_table(classification_results)

DataMining.Classification.Rate_Results.Calculate(classification_results)