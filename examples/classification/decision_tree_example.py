import os, sys, json
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

skiing = DataMining.Functions.read_json_from_file("examples\\test_data\\skiing.json")
computer_buyers = DataMining.Functions.read_json_from_file("examples\\test_data\\computer_buyers.json")
tennis = DataMining.Functions.read_json_from_file("examples\\test_data\\tennis.json")
sailing = DataMining.Functions.read_cat_csv_from_file("examples\\test_data\\sailing.csv", ";")
bank = DataMining.Functions.read_cat_csv_from_file("examples\\test_data\\bank.csv", ";")

data = bank

split_data = DataMining.Functions.split_data_in_n_parts(data, num=[9/10, 1/10], part_names = ["training","validation"])
#print(len(split_data["training"]), len(split_data["validation"]))

#DataMining.Functions.print_data_in_table(bank)

discrete_data = {}
discrete_data["complete"] = DataMining.Functions.discretize_values(data)
discrete_data["split"] = {}
discrete_data["split"]["training"] = DataMining.Functions.discretize_values(split_data["training"], discrete_data["complete"]["steps"])
discrete_data["split"]["validation"] = DataMining.Functions.discretize_values(split_data["validation"], discrete_data["complete"]["steps"])

#print(bank["DP40"])

# points = {
#     "Test_1":{'age': 30.0, 'job': 'unemployed', 'marital': 'married', 'education': 'primary', 'default': 'no', 'balance': 1787.0, 'housing': 'no', 'loan': 'no', 'contact': 'cellular', 'day': 19.0, 'month': 'oct', 'duration': 79.0, 'campaign': 1.0, 'pdays': -1.0, 'previous': 0.0, 'poutcome': 'unknown'},
#     "Test_2":{'age': 38.0, 'job': 'management', 'marital': 'single', 'education': 'tertiary', 'default': 'no', 'balance': 11971.0, 'housing': 'yes', 'loan': 'no', 'contact': 'unknown', 'day': 17.0, 'month': 'nov', 'duration': 609.0, 'campaign': 2.0, 'pdays': 101.0, 'previous': 3.0, 'poutcome': 'failure'}
#     }
# discrete_points = DataMining.Functions.discretize_values(points, discrete["steps"])
#print(discrete_points)


points_for_classification = {
        "A": {
            "weather":"sunny",
            "snowhight":">=50"
        },
        "B": {
            "weather":"rainy",
            "snowhight":"<50"
        },
        "C": {
            "weather":"snowy",
            "snowhight":"<50"
        }
    }

# points_for_classification = {
#     "X": {
#         "age":"youth",
#         "income":"medium",
#         "student":"yes",
#         "credit_rating":"fair"
#     }
# }

# points_for_classification = {
#     "A": {
#         "outlook":"sunny",
#         "temperature":"hot",
#         "humidity":"high",
#         "wind":"weak"
#     }    
# }

#decision_tree = DataMining.Classification.Decision_Trees.create_ID3(discrete_data["complete"]["data"]) 
decision_tree = DataMining.Classification.Decision_Trees.create_ID3(discrete_data["split"]["training"]["data"]) 
#decision_tree = DataMining.Classification.Decision_Trees.create_C4_5(discrete_data["split"]["training"]["data"]) 

#print(json.dumps(decision_tree, indent=4, sort_keys=True))

#print(DataMining.Classification.Decision_Trees.classificator(decision_tree, discrete_points["data"]["Test_2"]))

correct_classifications = 0

classification_results = {}

for point in discrete_data["split"]["validation"]["data"]:
    classification_results[point] = {}
    classification_results[point]["class"] = discrete_data["split"]["validation"]["data"][point]["class"] 
    result = DataMining.Classification.Decision_Trees.classificator(decision_tree, discrete_data["split"]["validation"]["data"][point])
    #print(point, result, discrete_data["split"]["validation"]["data"][point]["class"])
    classification_results[point]["guessed_class"] = result
    if result == discrete_data["split"]["validation"]["data"][point]["class"]:
        correct_classifications += 1

print(DataMining.Classification.Rate_Results.Calculate(classification_results))
print(correct_classifications/len(discrete_data["split"]["validation"]["data"]))