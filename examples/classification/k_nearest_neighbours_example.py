import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

data = {
    "DP1": [1,3],
    "DP2": [1,8],
    "DP3": [1,9],
    "DP4": [4,6],
    "DP5": [5,4],
    "DP6": [5,7],
    "DP7": [6,1],
    "DP8": [6,3],
    "DP9": [6,8],
    "DP10": [7,2],
    "DP11": [7,4],
    "DP12": [7,6],
    "DP13": [8,2],
    "DP14": [8,3]
}

classes = {
    'C1': ['DP1', 'DP2', 'DP3', 'DP4', 'DP6', 'DP9', 'DP12'], 
    'C2': ['DP5', 'DP7', 'DP8', 'DP10', 'DP11', 'DP13', 'DP14']
    }


#new_point = [10,12]
new_point = [6,6]
k_num = 10
results = DataMining.Classification.K_Nearest_Neighbours.Calculate(data, classes, new_point, k = k_num, distance_function = "Cityblock")
print(results)
name = "K-Nearest-Neighbours with k =" + str(k_num)
DataMining.Plotting.Clusters_2d(data, classes, plt_name=name, add_point = new_point, show_names=True)