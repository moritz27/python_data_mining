import os, sys
from beautifultable import BeautifulTable
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import DataMining

text_files = ["text_1.txt","text_2.txt"]
word_list = ["cyber","sicherheit", "bund", "studiengang", "richtlinie", "schinken"]

word_table = BeautifulTable()
word_table.rows.append(["Word", text_files[0], text_files[1]])
    
word_occurences = DataMining.Similarity.compare_texts(text_files, word_list = word_list, pathname = "examples\\texts")

for word in word_list:
    word_table.rows.append([word, \
                            word_occurences["text_1.txt"][word], \
                            word_occurences["text_2.txt"][word]])
    
print(word_table)

similarity = DataMining.Similarity.Cosine(word_occurences["text_1.txt"], word_occurences["text_2.txt"])
print("Cosine Similarity:", similarity)