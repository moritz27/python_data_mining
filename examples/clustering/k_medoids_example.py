import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining


data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")

k_num = 3
results = DataMining.Clustering.K_Medoids.Calculate(data, medoid_num = k_num)
print(results)
name = "K-Means with " + str(k_num) + " Centroids"
DataMining.Plotting.Clusters_2d(data, results["clusters"], centroids = results["best_medoids"], plt_name=name, show_names=True)
