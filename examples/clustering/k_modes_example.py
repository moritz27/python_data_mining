import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining


data = DataMining.Functions.read_json_from_file("examples\\test_data\\categorial_clustering.json")
data = DataMining.Functions.read_json_from_file("examples\\test_data\\mushroom.json")
DataMining.Functions.print_data_in_table(data)


k_num = 5
results = DataMining.Clustering.K_Modes.Calculate(data, mode_num = k_num)
# print(results)
# name = "K-Means with " + str(k_num) + " Centroids"
# DataMining.Plotting.Clusters_2d(data, results["clusters"], centroids = results["best_medoids"], plt_name=name, show_names=True)
