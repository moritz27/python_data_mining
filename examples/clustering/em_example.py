import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining


data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")
#data = DataMining.Functions.read_data_from_file("examples\\test_data\cluster.txt")
#bank = DataMining.Functions.read_cat_csv_from_file("examples\\test_data\\bank.csv", ";")


split_data = DataMining.Functions.split_data_in_n_parts(data, num=[9/10, 1/10], part_names = ["training","validation"])
print(len(split_data["training"]), len(split_data["validation"]))

#DataMining.Functions.split_data_in_n_parts(data, part_names = ["foo","bar","loo"])

# k_num = 3
# results = DataMining.Clustering.Expectation_Maximization.Calculate(data, centroid_num = k_num)
# print(results)
# name = "K-Means with " + str(k_num) + " Centroids"
# DataMining.Plotting.Clusters_2d(data, results["clusters"], centroids = results["centroids"], plt_name=name, show_names=True)