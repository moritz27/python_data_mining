import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining


data = {
    "A":[1,1],
    "B":[2,1],
    "C":[3,1],
    "D":[1,2],
    "E":[6,1],
    "F":[7,1],
    "G":[8,1]
}

#data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")
data = DataMining.Functions.read_data_from_file("examples\\test_data\\cluster.txt")
#data = DataMining.Functions.read_data_from_file("examples\\test_data\\sine_cluster.txt")


DataMining.Plotting.Plot_Points(data)

results = DataMining.Clustering.Optics.Calculate(data, min_pts = 100, distance_function = "Cityblock")
