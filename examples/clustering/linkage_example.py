import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

data = {
    "A":[1,1],
    "B":[2,2],
    "C":[4,3],
    "D":[4,4],
    "E":[6,4],
    "F":[8,5],
}


data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")
#data = DataMining.Functions.read_data_from_file("examples\\test_data\\cluster.txt", max_length = 300)


result = DataMining.Clustering.Linkage.Calculate(data, max_distance = 4, mode="mean", distance_function = "Cityblock", min_points_for_clusters = 1) #Cityblock
print(result)
DataMining.Plotting.Clusters_2d(data, result, plt_name="Single Linkage", show_names=True)