import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

# data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")
# epsilon = 2
# min_pts  = 2

data = DataMining.Functions.read_data_from_file("examples\\test_data\\cluster.txt")
epsilon = 40000
min_pts  = 100

#data = DataMining.Functions.read_data_from_file("examples\\test_data\\sine_cluster.txt")




#result = DataMining.Clustering.DB_Scan.Calculate(data, epsilon, min_pts)
result = DataMining.Clustering.DB_Scan_recursive.Calculate(data, epsilon, min_pts)
#print(result)
name = "DBSCAN with Epsilon = " + str(epsilon) + " and minPts = " + str(min_pts)
DataMining.Plotting.Clusters_2d(data, result, plt_name= name) #, show_names=True