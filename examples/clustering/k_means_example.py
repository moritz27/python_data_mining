import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining


#data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")
#data = DataMining.Functions.read_data_from_file("examples\\test_data\cluster.txt")
data = DataMining.Functions.convert_to_data_points([[1, 3, 6, 14, 17, 24, 26, 31],[1, 3, 6, 14, 17, 24, 26, 31]])
print(data)

k_num = 3
results = DataMining.Clustering.K_Means.Calculate(data, centroid_num = k_num)
print(results)
name = "K-Means with " + str(k_num) + " Centroids"
DataMining.Plotting.Clusters_2d(data, results["clusters"], centroids = results["centroids"], plt_name=name, show_names=True)