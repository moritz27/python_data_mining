import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining

#data = DataMining.Functions.read_json_from_file("examples\\test_data\\clique.json")
data = DataMining.Functions.read_json_from_file("examples\\test_data\\clustering.json")

clique_min_pts = 1
result = DataMining.Clustering.Hopkins_Statistic.Calculate(data, cell_step_sizes = [2,2])
print(result[0])
#result = DataMining.Clustering.CLIQUE_NEW.Calculate(data, min_pts = clique_min_pts)
#name = "CLIQUE MinPts=" + str(clique_min_pts)
#DataMining.Plotting.Clusters_2d(data, result, plt_name=name, show_names=True)

DataMining.Plotting.Plot_Points(data, add_points = result[1], show_names=True, show_add_names = True)
