import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import DataMining



#data = DataMining.Functions.read_json_from_file("examples\\test_data\\categorial_clustering.json")
#data = DataMining.Functions.read_json_from_file("examples\\test_data\\mushroom.json")
data = DataMining.Functions.read_cat_csv_from_file("examples\\test_data\\bank_small.csv", ";")

#print(data)

results = DataMining.Clustering.ROCK.Calculate(data)
print(results)