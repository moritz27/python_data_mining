import os, sys
from beautifultable import BeautifulTable
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import DataMining

# Cityblock, Euklid and Max
X = [22, 1, 42, 10]
Y = [20, 0, 36, 8]
print("a) Cityblock Norm:", DataMining.Distance.Cityblock(X,Y)) 
print("b) Euklid Norm:", DataMining.Distance.Euklid(X,Y))
print("c) Max Norm:", DataMining.Distance.Max_Norm(X,Y))